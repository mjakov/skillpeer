## Introduction

Welcome to SkillPeer, the platform for face-to-face knowledge sharing.

## Why learning communities are important?

Have you ever wanted to learn some practical skill like a foreign language but found
that book learning without practicing is not enough? Have you tried to find a tutor for your high-school math or science classes only to find out that private instruction is too expensive?

Online communities and forums can be free, but sometimes learning directly with other people can be best. Nothing beats the speed of face-to-face communication where you can pose questions and get your answers immediately. Add to this an online platform that groups people according to their interests and competence and you have got SkillPeer.

Education and face-to-face learning groups are the keys to solving many of the world's problems: from youth unemployment to the breakup of communities.

## Live demo
Here is a live [demo](http://www.skillpeer.pw) of the project.
It is based on open source tools like Ruby-on-Rails and JQuery Mobile.

## Join us
This project is being developed using an open-source model by Pro Mente d.o.o., a
small company in Zagreb, Croatia. We are always looking for web developers, designers,
and others who want to contribute. Even spreading the word can be great. If you are interested, just send us a short [email](mailto:miljenko.jakovljevic@gmail.com)
about what you would like to do.

No skills? No problem. SkillPeer gives you the perfect opportunity to learn. Other developers provide you with their mentoring and support.

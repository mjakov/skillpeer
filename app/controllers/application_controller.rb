class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :current_user, :logged_in?, :correct_facilitator?, :attending?
  before_action :set_cache_headers

  def current_user
    if session[:current_user_id].nil?
      return
    else
      @current_user ||= User.find_by_id(session[:current_user_id])
    end
  end

  def set_current_user(newuser)
    session[:current_user_id] = newuser.id
    @current_user = newuser
  end

  def logged_in?
    current_user.is_a? User
  end

  def correct_facilitator?(group)
    return (current_user && current_user.facilitator? && (group.facilitator_id == current_user.id))
  end

  def attending?(event)
    !((current_user.events.where(id: event.id)).empty?)
  end

  private

    def correct_facilitator
      if current_user
        @group = current_user.groups.find_by(id: params[:id])
        redirect_to root_url if @group.nil?
      else
        redirect_to root_url
      end
    end

    def set_cache_headers
      response.headers["Cache-Control"] = "no-cache, no-store"
      response.headers["Pragma"] = "no-cache"
      response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
    end

end # class ApplicationController

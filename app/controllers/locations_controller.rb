class LocationsController < ApplicationController

  def index

  	termStr = term_params[:term]
  	@locations = Location.order(:name).where("name like ?", "%#{termStr}%")
  	render json: @locations.map { |loc| loc.name+", "+loc.street+", "+loc.number+", "+loc.city+", "+loc.postnr+", "+loc.country }
  end

  private
  	def term_params
  		params.permit(:term)
  	end

end

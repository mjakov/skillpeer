class LoginsController < ApplicationController

  def new
    if current_user
      redirect_to root_url
    end
  end

  def create
    user = User.authenticate(login_params[:email], login_params[:password])
    if user
      session[:current_user_id] = user.id
      @current_user = user
      login_msg = "Welcome #{user.name.split()[0]}!"
      redirect_to root_url, :flash => { :success => login_msg }
    else
      flash.now[:danger] = "Login failed"
      render 'new'
    end
  end

  def destroy
    if current_user
      reset_session
      redirect_to root_url, :flash => { :info => "See you later!"}
    else
      redirect_to root_url
    end
  end

  private

    def login_params
      params.permit(:email, :password)
    end


end

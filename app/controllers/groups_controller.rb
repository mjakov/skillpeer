class GroupsController < ApplicationController
  before_action :correct_facilitator, only: [:edit, :destroy]

  helper_method [:correct_facilitator?, :attending?]

  def new
    if current_user && current_user.facilitator?
      @group = Group.new
      @location = @group.location
    else
      redirect_to root_url
    end
  end

  def create
    if current_user && current_user.facilitator?
      @location = Location.find_or_create_by(group_params[:location])
      @group = current_user.groups.build(group_params.except(:location))

      if @location.valid?
        @group.location = @location
        if @group.valid?
          @group.save
          flash[:success] = "Meeting group created successfully"
          redirect_to root_url
        else
          flash[:danger] = "Meeting group not created"
          render 'new'
        end
      else
        flash[:danger] = "Meeting group not created, invalid location"
        render 'new'
      end
    else
      redirect_to root_url
    end
  end#create

  def show
    @group = Group.find(params[:id])
  end

  def index
    @groups_hash = {}
    if current_user && current_user.facilitator?
      locationsGroups = Location.joins("INNER JOIN groups ON groups.location_id = locations.id AND groups.facilitator_id = #{current_user.id}").distinct.order("country ASC, city ASC")
    else
      locationsGroups = Location.joins("INNER JOIN groups ON groups.location_id = locations.id").
        distinct.order("country ASC, city ASC")
    end
    locationsGroups.each do |loc|
      if @groups_hash[loc.country].nil?
        @groups_hash[loc.country] = {}
      end
      if @groups_hash[loc.country][loc.city].nil?
        @groups_hash[loc.country][loc.city] = []
      end
      loc.groups.each do |grp|
        if current_user && current_user.facilitator?
          if grp.facilitator_id == current_user.id
            @groups_hash[loc.country][loc.city] << grp
          end
        else
          @groups_hash[loc.country][loc.city] << grp
        end
      end

    end
  end #index

  def edit
    if current_user && current_user.facilitator?
      @group = Group.find(params[:id])
      @location = @group.location
    end
  end

  def update
    if current_user && current_user.facilitator?
      @group = Group.find(params[:id])
      if @group && (@group.facilitator_id == current_user.id)
        @location = Location.find_or_create_by(group_params[:location])
        if @location
          @group.location = @location
          @group.save
        end
        @group = Group.update(@group.id, group_params.except(:location))

        if @group && @group.valid? && @location && @location.valid?
          flash[:success] = "Meeting Group Updated"
          redirect_to group_path(@group)
        else
          flash.now[:danger] = "Meeting Group Not Updated"
          render 'edit'
        end
      else
        redirect_to root_url
      end
    else
      redirect_to root_url
    end
  end

  def destroy
    @group.destroy
    flash[:success] = "Meeting group deleted"
    redirect_to groups_path
  end

  private

    def group_params
      params.require(:group).permit(:name, :max_places,
        :description, :area_size, :area_count, location: [:name,
          :country, :postnr, :city, :street, :number])
    end

end#class GroupsController

class SkillsController < ApplicationController

  before_action :correct_user, only: [:new, :create, :destroy]

  def new
    @user = User.find(params[:user_id])
    @skill = @user.skills.new
    @knowhows = Knowhow.order(:name)
  end

  def create
    @user = current_user
    @knowhows = Knowhow.all
    @skill = @user.skills.build(skill_params)
    @skill.validate
    if @skill.valid?
      @skill.save
      flash[:success] = "Skill created"
      redirect_to user_path(@user)
    else
      flash[:error] = "Skill not created"
      render :new
    end
  end

  def destroy
    skill = current_user().skills.find_by(id: params[:id])
    if skill.nil?
      redirect_to root_url
    else
      skill.destroy
      flash[:success] = "Skill deleted"
      redirect_to user_path(current_user)
    end
  end

  private

    def skill_params
      params.require(:skill).permit(:knowhow_id, :teaching)
    end

    def correct_user

      unless (current_user && (current_user.id == params[:user_id].to_i))
        redirect_to root_url
      end
    end

end

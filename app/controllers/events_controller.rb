class EventsController < ApplicationController

  require 'agenda/manager'
  include Agenda

  before_action :user_is_facilitator,
  only: [:create, :destroy, :edit, :update, :agenda, :visitors]

  before_action :correct_facilitator,
    only: [:create, :destroy, :edit, :update, :show, :agenda, :visitors]

  before_action :correct_group,
    only: [:destroy, :edit, :update, :attend, :show, :agenda, :visitors]

  def new
    @group = Group.find(params[:group_id])
    @event = @group.events.new
  end

  def create
    @group = Group.find(params[:group_id])
    @event = @group.events.build(event_params)
    @event.validate
    if @event.valid?
      @event.save
      flash[:success] = "Meeting event created"
      redirect_to @group
    else
      flash.now[:danger] = "Meeting event not created"
      render 'new'
    end
  end

  def edit
    @group = Group.find(params[:group_id])
    @event = @group.events.find_by(id: params[:id])
  end

  def update
    @group = Group.find(params[:group_id])
    @event = @group.events.find_by(id: params[:id])
    status = @event.update(event_params)
    if status
      flash[:success] = "Event update success"
      redirect_to group_path(@group)
    else
      flash.now[:danger] = "Event update failed"
      render 'edit'
    end
  end

  def show
    @group = Group.find(params[:group_id])
    @event = @group.events.find_by(id: params[:id])
    @learning_knowhows = @event.learning
    @teaching_knowhows = @event.teaching
    @visitors = []
    if group_facilitator?(@group, current_user)
      @visitors = @event.users
    end
  end

  def destroy
    @group = Group.find(params[:group_id])
    event = @group.events.find_by(id: params[:id])
    if event
      event.destroy
      flash[:success] = "Event deleted"
      redirect_to group_path(@group)
    else
      redirect_to root_url
    end
  end

  def attend
    @group = Group.find(params[:group_id])
    @event = @group.events.find_by(id: params[:id])
    if logged_in? && @event.free_places? && (! @event.attending?(current_user))
      @event.attend(current_user)
      flash[:success] = "Your name was added to the guest list."
      redirect_to group_event_path(@group, @event)
    else
      redirect_to root_url
    end
  end

  def unattend
    @group = Group.find(params[:group_id])
    @event = @group.events.find_by(id: params[:id])
    if @event && @event.attending?(current_user)
      @event.users.delete(current_user)
      flash[:success] = "Your name was removed from the guest list."
      redirect_to group_event_path(@group, @event)
    else
      redirect_to root_url
    end
  end

  def agenda
    @group = Group.find(params[:group_id])
    @event = @group.events.find_by(id: params[:id])
    if current_user && current_user.facilitator?
      manager = Manager.new(@group.area_size, @group.area_count)
      @event.users.each do |usr|
        manager.add_visitor(usr)
      end
      @agenda = manager.make_schedule
      if @agenda.empty?
        flash[:danger] = "Too few visitors, no agenda possible."
        redirect_to group_event_path(@group, @event)
      else
        render :agenda
      end
    end
  end

  def visitors
    @group = Group.find(params[:group_id])
    @event = @group.events.find_by(id: params[:id])
    @visitors = []
    if group_facilitator?(@group, current_user)
      @visitors = @event.users
    end
  end

  private

    def event_params
      params.require(:event).permit(:time_start, :time_end)
    end

    def group_facilitator?(group, user)
      user && group && user.facilitator? && group.facilitator_id == user.id
    end

    def correct_facilitator
      if current_user && current_user.facilitator?
        group = current_user.groups.find_by(id: params[:group_id])
        redirect_to root_url if group.nil?
      end
    end

    def user_is_facilitator
      unless current_user && current_user.facilitator?
        redirect_to root_url
      end
    end

    def correct_group
      gp = Group.find(params[:group_id])
      if gp.nil?
        redirect_to root_url
      else
        ev = gp.events.find_by(params[:id])
        if ev.nil?
          redirect_to root_url
        end
      end
    end

end#class EventsController

class UsersController < ApplicationController

  def new
    if current_user.nil?
      @user = User.new
    else
      redirect_to root_url
    end
  end

  def create
    @user = User.new(user_params)
    ###if verify_recaptcha(model: @user) && @user.save
    if @user.save      
      set_current_user(@user)
      msg = @user.name.split()[0] + ", please add some skills to your profile."
      flash[:success] = msg
      redirect_to user_path(current_user)
    else
      flash.now[:danger] = 'Please correct your registration data'
      render 'new'
    end
  end

  def show
    if (params[:id].to_i == session[:current_user_id]) ||
      (current_user && current_user.admin?)
      @user = User.find(params[:id])
    else
      redirect_to root_path
    end
  end

  def edit
    @user = User.find(params[:id])
    if (current_user != @user) && (! current_user.admin?)
      redirect_to root_url
    end
  end

  def update
    if current_user
      stt = current_user.update(user_params)
      if stt
        flash[:success] = "Profile update successful"
        redirect_to root_url
      else
        flash[:danger] = "Profile update failed"
        render 'edit'
      end
    end
  end

  def index
    if current_user && current_user.admin?
      @users = User.all
    else
      redirect_to root_url
    end
  end

  def enable_facilitator
    @user = User.find(params[:id])
    if current_user.admin? && @user.visitor?
      @user.make_facilitator
      @user.save
      redirect_to user_path(@user)
    else
      render 'show'
    end
  end

  def disable_facilitator
    @user = User.find(params[:id])
    if current_user.admin? && @user.facilitator?
      @user.unmake_facilitator
      @user.save
      redirect_to user_path(@user)
    else
      render 'show'
    end
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :password)
    end

end

class EventTimeValidator < ActiveModel::Validator

  DTIME_FORMAT = "%Y-%m-%d %H:%M"

  def validate(record)
    tStart = record.time_start
    tEnd = record.time_end
    mgId = record.group_id

    return if (tStart.nil? || tEnd.nil?)

    if tStart < DateTime.now
      record.errors.add(:time_start, "cannot be in the past")
    end

    if tEnd <= tStart
      record.errors.add(:time_end, "has to be after time start")
    end

    # Check old meeting event between new meeting event
    overlaps_start = Event.where({ group_id: mgId, time_start: tStart..tEnd })
    overlaps_end = Event.where({ group_id: mgId, time_end: tStart..tEnd })

    if (overlaps_start.count > 0 && overlaps_start.first.id != record.id)
      ovpEt = overlaps_start.first
      str1 = ovpEt.time_start.strftime(DTIME_FORMAT)
      str2 = ovpEt.time_end.strftime(DTIME_FORMAT)
      record.errors.add(:time_start, " - another event is "+
        "scheduled between #{str1} and #{str2}")
    end

    if (overlaps_end.count > 0 && overlaps_end.first.id != record.id)
      ovpEt = overlaps_end.first
      str1 = ovpEt.time_start.strftime(DTIME_FORMAT)
      str2 = ovpEt.time_end.strftime(DTIME_FORMAT)
      record.errors.add(:time_end, " - another event is " +
        "scheduled between #{str1} and #{str2}")
    end

    # Check new meeting event between old meeting event
    overlaps3 = Event.where(["group_id = ? and ((time_start < ?)"+
      "and (time_end > ?))", mgId, tStart, tEnd])

    if (overlaps3.count > 0 && overlaps3.first.id != record.id)
      ovpEt = overlaps3.first
      str1 = ovpEt.time_start.strftime(DTIME_FORMAT)
      str2 = ovpEt.time_end.strftime(DTIME_FORMAT)
      record.errors.add(:time_start, " - another event is scheduled " +
        "between #{str1} and #{str2}")
      record.errors.add(:time_end, " - another event is scheduled " +
        "between #{str1} and #{str2}")
    end

  end#validate

end#class

dtsClose = (dtStart, ui) ->
  console.log("dtsClose")
  $('#dtime_end').val(dtStart.toLocaleFormat("%Y/%m/%d %H:%M"))

jQuery ->
  console.log("events coffee ready!");
	$(document).on 'mouseenter', '#dtime_start', ->
    $('#dtime_start').datetimepicker({
       step: 15,
       onClose: dtsClose
     })
  $(document).on 'mouseenter', '#dtime_end', ->
    $('#dtime_end').datetimepicker({
       step: 15
     })

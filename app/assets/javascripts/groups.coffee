$ ->
	console.log("groups.coffee: DOM is ready!")

	selectHandler = (evt, ui) ->
		evt.preventDefault();
		console.log("selectHandler called:")
		console.log("value selected: " + ui.item.value)
		locTokens = ui.item.value.split(", ")
		console.log(locTokens)
		$('#group_location_name').val(locTokens[0])
		$('#group_location_street').val(locTokens[1])
		$('#group_location_number').val(locTokens[2])
		$('#group_location_city').val(locTokens[3])
		$('#group_location_postnr').val(locTokens[4])
		$('#group_location_country').val(locTokens[5])

	focusHandler = (evt, ui) ->
		console.log("Focus Handler")

	changeHandler = (evt, ui) ->
		console.log("Change Handler")

	$(document).on 'click', '#group_location_name', ->
		$('#group_location_name').autocomplete(
			source: $('#group_location_name').data('autocomplete-source'),
			focus: focusHandler,
			change: changeHandler,
			select: selectHandler)

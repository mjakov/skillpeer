module Agenda
  class Manager

    attr_accessor :area_size, :area_count
    attr_reader :visitor_list
    attr_reader :tribe_list

    def initialize(area_size, area_count)
      @area_size = area_size
      @area_count = area_count
      @visitor_list = []
      @tribe_list = []
    end

    def add_visitor(visitor)
      @visitor_list << visitor
    end

    def get_tribes_by_knowhow(knowhow_id)
      found_tribes = []
      for g in @tribe_list
        if g.knowhow_id == knowhow_id
          found_tribes << g
        end
      end
      return found_tribes
    end

    def tribe_count
      @tribe_list.length
    end

    def visitor_count
      @visitor_list.length
    end

    # Returns a schedule matrix where each row corresponds to a round
    # and each column to a learning area. Each matrix entry contains the
    # scheduled knowhow.
    def make_schedule
      form_tribes()
      eliminate_unfeasible_tribes()
      split_too_big_tribes()
      round_count = calc_min_rounds
      slot_count = round_count * @area_count
      split_additional_tribes(slot_count)
      optimizer = Optimizer.new(round_count, @area_count, @tribe_list)
      schedule = optimizer.make_schedule
      return schedule
    end

    private

      def add_tribe(tribe)
        @tribe_list << tribe
        return tribe
      end

      def reset_tribes
        @tribe_list.clear
      end

      def form_tribes
        reset_tribes
        for v in @visitor_list
          for s in v.skills
            g = get_tribes_by_knowhow(s.knowhow_id).first
            if g.nil?
              g = add_tribe(Tribe.new(s.knowhow_id))
            end
            if s.teaching
              g.add_teacher(v)
            else
              g.add_learner(v)
            end
          end
        end
      end

      def eliminate_unfeasible_tribes
        @tribe_list.select! { |g| g.feasible? }
      end

      def too_big_tribes?
        exBgGps = false
        for g in @tribe_list
          if g.too_big?(@area_size)
            exBgGps = true
            break
          end
        end
        return exBgGps
      end

      # Recursively splits too big tribes
      def Manager.splitG(gLst, area_size)
        case gLst.length
        when 0
          return gLst
        when 1
          g = gLst[0]
          if g.too_big?(area_size)
            return splitG([g.split] + gLst, area_size)
          else
            return gLst
          end
        else
          hlf = gLst.length.div(2)
          rst = gLst.length - hlf
          newLst1 = gLst.slice(0, hlf)
          newLst2 = gLst.slice(hlf, rst)
          return (splitG(newLst1, area_size) + splitG(newLst2, area_size))
        end
      end

      # Post: all tribes size estimates are smaller than area size
      def split_too_big_tribes
        @tribe_list = Manager.splitG(@tribe_list, @area_size)
      end

      # Calculates minimum number of rounds needed to provide enough slots for
      # all the tribes
      def calc_min_rounds
        if tribe_count < 1
          return 0
        else
          (tribe_count.to_f / area_count.to_f).ceil
        end
      end

      # Splits additional splittable tribes to fill all the slots
      def split_additional_tribes(slots_cnt)
        @tribe_list.sort! { |x,y| y.size_estimate <=> x.size_estimate }
        free_places = slots_cnt - tribe_count
        for g in @tribe_list
          break unless free_places > 0
          if g.splittable?
            newGrp = g.split
            add_tribe(newGrp)
            free_places -= 1
          end
        end
      end

    # end private

  end #class Manager

end #module Agenda

module Agenda

  class Tribe
    attr_reader :knowhow_id
    attr_reader :learner_list, :teacher_list

    MIN_TEACHERS = 1
    MIN_LEARNERS = 1

    def initialize(knowhow_id)
      @knowhow_id = knowhow_id
      @learner_list = []
      @teacher_list = []
    end

    def add_learner(learner)
      @learner_list << learner
    end

    def add_teacher(teacher)
      @teacher_list << teacher
    end

    def feasible?
      (teacher_count >= MIN_TEACHERS) and
      (learner_count >= MIN_LEARNERS)
    end

    def size_estimate
      learner_count + [learner_count, teacher_count].min
    end

    def too_big?(area_size)
      size_estimate > area_size
    end

    def learner_count
      @learner_list.length
    end

    def teacher_count
      @teacher_list.length
    end

    # A tribe is splittable if the number of teachers and learners is greater
    # than one.
    def splittable?
      (teacher_count > 1) and (learner_count > 1)
    end

    def split
      newGp = Tribe.new(@knowhow_id)
      cntTchs = teacher_count().div(2)
      cntLrns = learner_count().div(2)
      for i in (1..cntTchs)
        newGp.add_teacher(@teacher_list.shift)
      end
      for i in (1..cntLrns)
        newGp.add_learner(@learner_list.shift)
      end
      return newGp
    end

  end # class Tribe

end # module Agenda

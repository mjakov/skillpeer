module Agenda

  class Optimizer

    attr_reader :schedule
    attr_reader :round_cnt, :area_cnt, :tribes

    def initialize(round_cnt, area_cnt, tribes)
      @round_cnt = round_cnt
      @area_cnt = area_cnt
      @tribes = tribes
      @schedule = Array.new(@round_cnt) { Array.new(@area_cnt)}
    end

    def Optimizer::schedule_inspect(schedule)
      str = "Schedule:\n"
      for rd in schedule
        for gp in rd
          if gp.nil?
            str.concat("nil ")
          else
            str.concat(gp.knowhow_id.to_s)
            str.concat(" ")
          end
        end
        str.concat("\n")
      end
      return str
    end

    # Returns schedule for an event. It is a matrix round x area with tribe entries
    def make_schedule
      @tribes.sort! {|x,y| x.knowhow_id <=> y.knowhow_id}
      ltKgId = nil
      lastArea = Array.new(@round_cnt) {0}
      rnd = 0
      for gp in @tribes
        if ltKgId == gp.knowhow_id
          begin
            rnd = (rnd + 1) % @round_cnt
          end until lastArea[rnd] < @area_cnt
        elsif ltKgId != nil
          while lastArea[rnd] >= @area_cnt
            rnd = (rnd + 1) % @round_cnt
          end
        end
        @schedule[rnd][lastArea[rnd]] = gp
        lastArea[rnd] += 1
        ltKgId = gp.knowhow_id
      end
      return @schedule
    end

  end #Optimizer

end # module Agenda

class User < ApplicationRecord

  has_many :groups, { :foreign_key => 'facilitator_id' }

  has_and_belongs_to_many :events

  has_many :skills

  validates :name, presence: true, length: {minimum: 4, maximum: 30}
  validates :password, presence: true, length: {minimum: 5, maximum: 30}

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\-.]+\.[a-z]+\z/i

  validates :email, format: {with: VALID_EMAIL_REGEX,
      message: "address is not valid"}, length: {maximum: 50},
      uniqueness: true

  ROLE = {:VISITOR => 1, :FACILITATOR => 2, :ADMIN => 3, :BANNED => 0 }
  ROLE_STR = {:VISITOR => "Visitor", :FACILITATOR => "Facilitator",
    :ADMIN => "Admin"}

  def User.authenticate(emailStr, pwdStr)
    user = User.find_by(email: emailStr, password: pwdStr)
  end

  # Returns role code from the array of role symbols
  def User.roles(roleSyms)
    val = 0
    for roleSym in roleSyms
      val |= 2 ** ROLE[roleSym]
    end
    return val
  end

  def role_str
    if admin?
      return ROLE_STR[:ADMIN]
    elsif facilitator?
      return ROLE_STR[:FACILITATOR]
    elsif visitor?
      return ROLE_STR[:VISITOR]
    end
  end

  def admin?
    ((role >> ROLE[:ADMIN]) & 1) == 1
  end

  def facilitator?
    ((role >> ROLE[:FACILITATOR]) & 1) == 1
  end

  def visitor?
    ((role >> ROLE[:VISITOR]) & 1) == 1
  end

  def make_facilitator
    self.role |= (2 ** ROLE[:FACILITATOR])
  end

  def unmake_facilitator
    self.role &= ~(2**ROLE[:FACILITATOR])
  end

  def banned?
    ((role >> ROLE[:BANNED]) & 1) == 1
  end

  def learns(knowhow)
    self.skills.create(knowhow_id: knowhow.id)
  end

  def learns_by_id(knowhow_id)
    self.skills.create(knowhow_id: knowhow_id)
  end

  def teaches(knowhow)
    self.skills.create(knowhow_id: knowhow.id, teaching: true)
  end

  def teaches_by_id(knowhow_id)
    self.skills.create(knowhow_id: knowhow_id, teaching: true)
  end

end#User

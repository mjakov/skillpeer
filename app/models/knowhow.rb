class Knowhow < ApplicationRecord

  has_many :skills

  validates :name, presence: true, length: {minimum: 4, maximum: 30},
    uniqueness: true

end

class Location < ApplicationRecord

  has_many :groups

  validates :name, presence: true, length: {minimum: 7, maximum: 50}
  validates :country, presence: true, length: {minimum: 3, maximum: 60}
  validates :postnr, presence: true, length: { minimum: 3 }
  validates :city, presence: true, length: {minimum: 3, maximum: 50}
  validates :street, presence: true, length: {minimum: 5, maximum: 50}
  validates :number, presence: true, length: {minimum: 1, maximum: 10}

  validates_uniqueness_of :name, scope: [:country, :postnr]

end

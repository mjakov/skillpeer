class Group < ApplicationRecord

  after_initialize :init

  belongs_to :facilitator, { :foreign_key => 'facilitator_id',
    :class_name => 'User'}

  belongs_to :location

  has_many :events, dependent: :destroy

  validates :name, presence: true, length: {minimum: 7, maximum: 50}

  validates :name, uniqueness: { scope: :location_id,
    message: "only one event with this name at your location"}

  validates :max_places, presence: true, numericality:
    {greater_than_or_equal_to: 0, only_integer: true}

  validates :description, length: { maximum: 1000 }

  validates :facilitator_id, presence: true, numericality:
    {greater_than_or_equal_to: 0, only_integer: true}

  validates :area_size, presence: true, numericality:
    {greater_than_or_equal_to: 2, only_integer: true}

  validates :area_count, presence: true, numericality:
    {greater_than_or_equal_to: 1, only_integer: true}

  def build_location
    deftLoc = Location.where(name: "Default").first
    if deftLoc
      return deftLoc
    else
      return Location.create!(name: "Default", country: "Default",
        postnr: 100, city: "Default", street: "Default", number: "Default")
    end
  end

  def init
    self.location ||= build_location
  end

end#class Group

class Skill < ApplicationRecord
  belongs_to :user
  belongs_to :knowhow

  validates :knowhow_id, uniqueness: { scope: :user_id, message: "should be unique" }

end

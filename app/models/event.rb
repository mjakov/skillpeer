class Event < ApplicationRecord

  include ActiveModel::Validations

  belongs_to :group

  has_and_belongs_to_many :users

  validates :group_id, presence: true
  validates :time_start, presence: true
  validates :time_end, presence: true

  # DEBUG: Removed this validation for the time being since time_start and time_end apparently
  # have DateTime or TimeWithZone type
  #validates_format_of [:time_start, :time_end], with: /20\d\d-[01][1-9]-[0-3]\d [0-2]\d:[0-6]\d/

  validates_with EventTimeValidator

  def free_places?
    ((group.max_places - users.count) > 0)
  end

  def free_places
    group.max_places - users.count
  end

  def attending?(user)
    users.find_by(id: user.id)
  end

  def attend(user)
    self.users << user
  end

  # Gives the statistic of skills that are taught
  def teaching
    Knowhow.joins("JOIN skills, events_users ON knowhows.id=skills.knowhow_id \
    AND skills.teaching='t' AND skills.user_id=events_users.user_id \
    AND events_users.event_id=#{self.id}").group("knowhows.name").count
  end

  # Gives the statistic of skills that are learned
  def learning
    Knowhow.joins("JOIN skills, events_users ON knowhows.id=skills.knowhow_id \
    AND skills.teaching='f' AND skills.user_id=events_users.user_id \
    AND events_users.event_id=#{self.id}").group("knowhows.name").count
  end


end

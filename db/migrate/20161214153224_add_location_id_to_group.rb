class AddLocationIdToGroup < ActiveRecord::Migration[5.0]
  def change
    change_table(:groups) do |t|
      t.references :location
    end    
  end
end

class ChangeLocationPostnrToString < ActiveRecord::Migration[5.0]
  def change
  	change_column :locations, :postnr, :string
  end
end

class CreateEventsUsersJoinTable < ActiveRecord::Migration[5.0]
  def change
    create_join_table(:events, :users, force: true) do |t|
      t.index [:event_id, :user_id], unique: true
    end
  end
end

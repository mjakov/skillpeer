class CreateLocations < ActiveRecord::Migration[5.0]
  def change
    create_table :locations do |t|
      t.string :name
      t.string :country
      t.integer :postnr
      t.string :city
      t.string :street
      t.string :number

      t.timestamps
    end
    add_index :locations, :name
    add_index :locations, [:city, :name]
    add_index :locations, [:postnr, :name]
    add_index :locations, [:country, :city, :name]
    add_index :locations, [:country, :postnr, :name]
  end
end

class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.integer :group_id
      t.datetime :time_start
      t.datetime :time_end

      t.timestamps
    end
  end
end

class CreateKnowhows < ActiveRecord::Migration[5.0]
  def change
    create_table :knowhows do |t|
      t.string :name

      t.timestamps
    end
    add_index :knowhows, :name, unique: true
  end
end

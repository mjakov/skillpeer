class CreateGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :groups do |t|
      t.string :name
      t.string :location
      t.string :address
      t.integer :max_places
      t.text :description

      t.timestamps
    end
  end
end

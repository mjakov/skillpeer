class AddAreaToGroup < ActiveRecord::Migration[5.0]
  def change
    add_column :groups, :area_size, :integer
    add_column :groups, :area_count, :integer
  end
end

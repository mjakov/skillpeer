class CreateSkills < ActiveRecord::Migration[5.0]
  def change
    create_table :skills do |t|
      t.references :user, foreign_key: true
      t.references :knowhow, foreign_key: true
      t.boolean :teaching, default: false

      t.timestamps
    end
    add_index(:skills, [:user_id, :knowhow_id], unique: true)
  end
end

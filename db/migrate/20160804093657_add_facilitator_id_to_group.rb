class AddFacilitatorIdToGroup < ActiveRecord::Migration[5.0]
  def change
    add_column :groups, :facilitator_id, :integer
  end
end

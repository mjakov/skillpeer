# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170108191547) do

  create_table "events", force: :cascade do |t|
    t.integer  "group_id"
    t.datetime "time_start"
    t.datetime "time_end"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "events_users", id: false, force: :cascade do |t|
    t.integer "event_id", null: false
    t.integer "user_id",  null: false
    t.index ["event_id", "user_id"], name: "index_events_users_on_event_id_and_user_id", unique: true
  end

  create_table "groups", force: :cascade do |t|
    t.string   "name"
    t.integer  "max_places"
    t.text     "description"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "facilitator_id"
    t.integer  "area_size"
    t.integer  "area_count"
    t.integer  "location_id"
    t.index ["location_id"], name: "index_groups_on_location_id"
  end

  create_table "knowhows", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_knowhows_on_name", unique: true
  end

  create_table "locations", force: :cascade do |t|
    t.string   "name"
    t.string   "country"
    t.string   "postnr"
    t.string   "city"
    t.string   "street"
    t.string   "number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["city", "name"], name: "index_locations_on_city_and_name"
    t.index ["country", "city", "name"], name: "index_locations_on_country_and_city_and_name"
    t.index ["country", "postnr", "name"], name: "index_locations_on_country_and_postnr_and_name"
    t.index ["name"], name: "index_locations_on_name"
    t.index ["postnr", "name"], name: "index_locations_on_postnr_and_name"
  end

  create_table "skills", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "knowhow_id"
    t.boolean  "teaching",   default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["knowhow_id"], name: "index_skills_on_knowhow_id"
    t.index ["user_id", "knowhow_id"], name: "index_skills_on_user_id_and_knowhow_id", unique: true
    t.index ["user_id"], name: "index_skills_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email"
    t.string   "name"
    t.string   "password"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "role",       default: 2
    t.index ["email"], name: "index_users_on_email", unique: true
  end

end

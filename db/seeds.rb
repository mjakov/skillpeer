# coding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

############################################
## Deployment branch - production version ##
############################################

cro = Knowhow.create!(name: "Croatian")
ger = Knowhow.create!(name: "German")
eng = Knowhow.create!(name: "English")
fre = Knowhow.create!(name: "French")
ita = Knowhow.create!(name: "Italian")
rus = Knowhow.create!(name: "Russian")
chi = Knowhow.create!(name: "Chinese")
jap = Knowhow.create!(name: "Japanese")
spa = Knowhow.create!(name: "Spanish")
por = Knowhow.create!(name: "Portuguese")
man = Knowhow.create!(name: "Mandarin")
pol = Knowhow.create!(name: "Polish")
heb = Knowhow.create!(name: "Hebrew")
swe = Knowhow.create!(name: "Swedish")
ire = Knowhow.create!(name: "Irish")
cze = Knowhow.create!(name: "Czech")
ara = Knowhow.create!(name: "Arabic")
per = Knowhow.create!(name: "Persian")

# mat1 = Knowhow.create!(name: "Math 1st Grade Highschool ")
# mat2 = Knowhow.create!(name: "Math 2nd Grade Highschool")
# mat3 = Knowhow.create!(name: "Math 3rd Grade Highschool")
# mat4 = Knowhow.create!(name: "Math 4th Grade Highschool")
#
# phy1 = Knowhow.create!(name: "Physics 1st Grade Highschool")
# phy2 = Knowhow.create!(name: "Physics 2nd Grade Highschool")
# phy3 = Knowhow.create!(name: "Physics 3rd Grade Highschool")
# phy4 = Knowhow.create!(name: "Physics 4th Grade Highschool")
#
# cs1 = Knowhow.create!(name: "Computer Science 1 Highschool")
# cs2 = Knowhow.create!(name: "Computer Science 2 Highschool")
# cs3 = Knowhow.create!(name: "Computer Science 3 Highschool")
# cs4 = Knowhow.create!(name: "Computer Science 4 Highschool")
#
# che1 = Knowhow.create!(name: "Chemistry 1st Grade Highschool")
# che2 = Knowhow.create!(name: "Chemistry 2nd Grade Highschool")
# che3 = Knowhow.create!(name: "Chemistry 3rd Grade Highschool")
# che4 = Knowhow.create!(name: "Chemistry 4th Grade Highschool")


langs1 = [cro, ger, eng, fre]
langs2 = [ita, rus, chi]


# Locations

default_location = Location.create(name: "Default", country: "Default",
  postnr: "1000", city: "Default", street: "Default", number: "1a")

loc1 = Location.create(name: "Pivnica Pinta", country: "Croatia",
  postnr: "10000", city: "Zagreb", street: "Radićeva Ulica", number: "3a")

loc2 = Location.create!(name: "Pivnica Zlatni Medo", country: "Croatia",
  postnr: "10000", city: "Zagreb", street: "Tkalciceva", number: "5")
loc3 = Location.create!(name: "Cafe Ban", country: "Croatia",
  postnr: "10000", city: "Rijeka", street: "Trg Bana Jelacica", number: "10")
loc4 = Location.create!(name: "Zlatna Zvona", country: "Croatia",
  postnr: "10000", city: "Rijeka", street: "Trg Bana Jelacica", number: "10")
loc5 = Location.create!(name: "Cafe Central", country: "Hungary",
  postnr: "10000", city: "Budapest", street: "Trg Bana Jelacica", number: "10")
loc6 = Location.create!(name: "Cafe Zriny", country: "Hungary",
  postnr: "10000", city: "Budapest", street: "Trg Bana Jelacica", number: "10")
#not used
loc7 = Location.create!(name: "Cafe Zriny", country: "Hungary",
    postnr: "10010", city: "Gyor", street: "Trg Bana Jelacica", number: "10")
loc8 = Location.create!(name: "Hard Rock Cafe", country: "Slovenia",
    postnr: "10000", city: "Maribor", street: "Trg Bana Jelacica", number: "10")

# Users
pintaUser = User.create(name: "Ivan Protrka", email: "protrka@gmail.com",
     password: "volimpintu")

thomas_jefferson = User.create!(name: "Thomas Jefferson", email: "thomas@example.com",
    password: "somepassword1")

tom_sawyer = User.create!(name: "Tom Sawyer", email: "tomsawyer@example.com",
    password: "somepassword1", role: User.roles([:FACILITATOR]) )

mike_tyson = User.create!(name: "Mike Tyson", email: "miketyson@example.com",
    password: "somepassword1", role: User.roles([:VISITOR, :FACILITATOR]) )

nonvisitor = User.create!(name: "Non Visitor", email: "nonvisitor@example.com",
        password: "somepassword1", role: 0 )

august_senoa = User.create!(name: "August Senoa", email: "agsenoa@example.com",
    password: "somepassword1", role: User.roles([:VISITOR]) )

admin = User.create!(name: "King Kong", email: "admin@example.com",
    password: "somepassword1", role: User.roles([:ADMIN]))

visitors = []
for i in 1..10
  newUsr = User.create!(name: "Visitor #{i}", email: "visitor#{i}@example.com",
    password: "somepassword1")
  for lang in langs1.sample(2)
    newUsr.learns(lang)
  end
  for lang in langs2.sample(2)
    newUsr.teaches(lang)
  end
  visitors << newUsr
end
for i in 11..20
  newUsr = User.create!(name: "Visitor #{i}", email: "visitor#{i}@example.com",
    password: "somepassword1")
  for lang in langs1.sample(2)
    newUsr.teaches(lang)
  end
  for lang in langs2.sample(2)
    newUsr.learns(lang)
  end
  visitors << newUsr
end

newUsr = User.create!(name: "Visitor 21", email: "visitor21@example.com",
  password: "somepassword1")
newUsr.learns(jap)
newUsr.teaches(eng)
visitors << newUsr

fakedesc = Faker::Lorem.sentence(100)
meetingGp1 = Group.create!(name: "Language Conversation Table", max_places: 30,
  description: fakedesc, facilitator_id: tom_sawyer.id,
  area_size: 6, area_count: 5)

meetingGp1.location = loc1
meetingGp1.save

meetingGp2 = Group.create!(name: "Babilon Cafe", max_places: 30,
    description:fakedesc, facilitator_id: tom_sawyer.id,
    area_size: 5, area_count: 6)
meetingGp2.location = loc2
meetingGp2.save

meetingGp3 = Group.create!(name: "CKT Cafe", max_places: 30,
    description: fakedesc, facilitator_id: mike_tyson.id,
    area_size: 3, area_count: 10)
meetingGp3.location = loc3
meetingGp3.save

meetingGp4 = Group.create!(name: "Langotango", max_places: 30,
    description: fakedesc, facilitator_id: mike_tyson.id,
    area_size: 3, area_count: 10)
meetingGp4.location = loc4
meetingGp4.save

meetingGp5 = Group.create!(name: "Language Connection", max_places: 30,
    description: fakedesc, facilitator_id: mike_tyson.id,
    area_size: 3, area_count: 10)
meetingGp5.location = loc5
meetingGp5.save

meetingGp6 = Group.create!(name: "Meetolingo", max_places: 30,
    description: fakedesc, facilitator_id: mike_tyson.id,
    area_size: 3, area_count: 10)
meetingGp6.location = loc6
meetingGp6.save

meetingGp1Ev1 = Event.create!(group_id: meetingGp1.id,
  time_start: DateTime.parse("2099-11-02 17:00"), time_end: DateTime.parse("2099-11-02 20:00"))

meetingGp1Ev2 = Event.create!(group_id: meetingGp1.id,
  time_start: DateTime.parse("2099-11-05 17:00"), time_end: DateTime.parse("2099-11-05 20:00"))

meetingGp1Ev3 = Event.create!(group_id: meetingGp1.id,
  time_start: DateTime.parse("2099-12-05 17:00"), time_end: DateTime.parse("2099-12-05 20:00"))

meetingGp1Ev4 = Event.create!(group_id: meetingGp1.id,
  time_start: DateTime.parse("2099-12-08 17:00"), time_end: DateTime.parse("2099-12-08 20:00"))

meetingGp3Ev1 = Event.create!(group_id: meetingGp3.id,
  time_start: DateTime.parse("2099-12-08 17:00"), time_end: DateTime.parse("2099-12-08 20:00"))

tom_sawyer.skills.create(knowhow_id: cro.id, teaching: true)
tom_sawyer.skills.create(knowhow_id: ger.id)
mike_tyson.skills.create(knowhow_id: cro.id, teaching: true)
mike_tyson.skills.create(knowhow_id: ger.id)
thomas_jefferson.skills.create(knowhow_id: ger.id, teaching: true)
august_senoa.learns(ger)
august_senoa.learns(cro)


meetingGp1Ev1.attend(tom_sawyer)
meetingGp1Ev1.attend(thomas_jefferson)
meetingGp1Ev1.attend(august_senoa)
meetingGp1Ev2.attend(mike_tyson)
for vis in visitors
  meetingGp1Ev4.attend(vis)
end

Rails.application.routes.draw do

  get 'static_pages/home'
  get 'static_pages/whatis'
  get 'static_pages/learners'
  get 'static_pages/teachers'
  get 'static_pages/organizers'
  get 'static_pages/contributors'
  get 'static_pages/media'

  root :to => 'groups#index'

  resources :users do
    resources :skills, only: [:new, :create, :destroy]
  end

  post 'users/:id/enable_facilitator', to: 'users#enable_facilitator', as: 'enable_facilitator'
  post 'users/:id/disable_facilitator', to: 'users#disable_facilitator', as: 'disable_facilitator'

  resource :login

  resources :groups do
    resources :events, only: [:create, :new, :edit, :update, :destroy, :show]
  end

  post 'groups/:group_id/events/:id/attend', to: 'events#attend', as: 'attend_event'
  post 'groups/:group_id/events/:id/unattend', to: 'events#unattend', as: 'unattend_event'

  post 'groups/:group_id/events/:id/agenda', to: 'events#agenda', as: 'create_agenda'

  get 'groups/:group_id/events/:id/visitors', to: 'events#visitors', as: "event_visitors"

  resources :locations, only: [:index]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

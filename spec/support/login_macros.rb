# Login helper macros for feature tests
module LoginMacros

  def login_as(user)
    session[:current_user_id] = user.id
  end

  def logout
    session[:current_user_id] = nil
  end


  # For feature tests
  def sign_in(user)
    visit root_path
    click_link 'Login'
    fill_in 'email', with: user.email
    fill_in 'password', with: user.password
    click_button "Log in"
  end

  def sign_out
    visit root_path
    click_button 'Logout'
  end

end

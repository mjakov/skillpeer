module EventSetup

  def setup_user_skill_events_2
    @thomas_jefferson = User.create(name: "Thomas Jefferson", email: "thomas@example.com",
        password: "somepassword1")
    @tom_sawyer = User.create(name: "Tom Sawyer", email: "tomsawyer@example.com",
        password: "somepassword1", role: User.roles([:VISITOR, :FACILITATOR]) )
    @mike_tyson = User.create(name: "Mike Tyson", email: "miketyson@example.com",
        password: "somepassword1", role: User.roles([:VISITOR, :FACILITATOR]) )
    @mark_twain = User.create(name: "Mark Twain", email: "marktwain@example.com",
        password: "somepassword1", role: User.roles([:VISITOR]) )

    @location1 = build(:location)
    @location1.country = "Croatia"
    @location1.city = "Zagreb"
    @location1.save

    @location2 = build(:location)
    @location2.country = "Croatia"
    @location2.city = "Split"
    @location2.save

    @meetingGp1 = Group.create!(name: "Language Conversation Table A", max_places: 30,
      description: "We meet Sundays at 5 P.M.", facilitator_id: @tom_sawyer.id,
      area_size: 6, area_count: 5)
    @meetingGp1.location = @location1
    @meetingGp1.save
    @meetingGp2 = Group.create!(name: "Babilon Cafe A", max_places: 30,
      description: "We meet Thursdays at 7 P.M.", facilitator_id: @mike_tyson.id,
      area_size: 6, area_count: 5)
    @meetingGp2.location = @location2
    @meetingGp2.save

    @meetingGp1Ev1 = Event.create(group_id: @meetingGp1.id,
      time_start: DateTime.parse("2099-11-02 17:00"), time_end: DateTime.parse("2099-11-02 20:00"))
    @meetingGp1Ev2 = Event.create(group_id: @meetingGp1.id,
        time_start: DateTime.parse("2099-11-05 17:00"), time_end: DateTime.parse("2099-11-05 20:00"))
    @meetingGp1Ev3 = Event.create(group_id: @meetingGp1.id,
        time_start: DateTime.parse("2099-11-07 17:00"), time_end: DateTime.parse("2099-11-07 20:00"))

    @meetingGp2Ev1 = Event.create(group_id: @meetingGp2.id,
        time_start: DateTime.parse("2099-11-02 17:00"), time_end: DateTime.parse("2099-11-02 20:00"))
    @meetingGp2Ev2 = Event.create(group_id: @meetingGp2.id,
        time_start: DateTime.parse("2099-11-05 17:00"), time_end: DateTime.parse("2099-11-05 20:00"))
    @meetingGp2Ev3 = Event.create(group_id: @meetingGp2.id,
        time_start: DateTime.parse("2099-11-08 17:00"), time_end: DateTime.parse("2099-11-08 20:00"))

    @cro = Knowhow.create(name: "Croatian")
    @ger = Knowhow.create(name: "German")
    @eng = Knowhow.create(name: "English")
    @fre = Knowhow.create(name: "French")
    @ita = Knowhow.create(name: "Italian")
    @rus = Knowhow.create(name: "Russian")
    @chi = Knowhow.create(name: "Chinese")

    @thomas_jefferson.skills.create(knowhow_id: @eng.id, teaching: true)
    @thomas_jefferson.skills.create(knowhow_id: @fre.id)
    @thomas_jefferson.skills.create(knowhow_id: @ger.id)

    @tom_sawyer.skills.create(knowhow_id: @cro.id)
    @tom_sawyer.skills.create(knowhow_id: @ger.id)
    @tom_sawyer.skills.create(knowhow_id: @eng.id, teaching: true)

    @mike_tyson.skills.create(knowhow_id: @cro.id, teaching: true)
    @mike_tyson.skills.create(knowhow_id: @ger.id)
    @mike_tyson.skills.create(knowhow_id: @eng.id)

    @mark_twain.skills.create(knowhow_id: @ger.id)
    @mark_twain.skills.create(knowhow_id: @fre.id, teaching: true)
    @mark_twain.skills.create(knowhow_id: @eng.id, teaching: true)

    @meetingGp1Ev1.attend(@thomas_jefferson)
    @meetingGp1Ev1.attend(@tom_sawyer)
    @meetingGp1Ev1.attend(@mike_tyson)
    @meetingGp1Ev1.attend(@mark_twain)

    @meetingGp1Ev2.attend(@thomas_jefferson)
    @meetingGp1Ev2.attend(@tom_sawyer)
    @meetingGp1Ev2.attend(@mark_twain)

    @meetingGp1Ev3.attend(@tom_sawyer)
    @meetingGp1Ev3.attend(@mike_tyson)
    @meetingGp1Ev3.attend(@mark_twain)

    @meetingGp2Ev1.attend(@thomas_jefferson)
    @meetingGp2Ev1.attend(@tom_sawyer)

    @meetingGp2Ev1.attend(@mike_tyson)
    @meetingGp2Ev1.attend(@mark_twain)

    @meetingGp2Ev3.attend(@mark_twain)
  end#setup_user_skill_events_2

  def destroy_user_skill_events_2
    @thomas_jefferson.destroy
    @tom_sawyer.destroy
    @mike_tyson.destroy
    @mark_twain.destroy

    @meetingGp1.destroy
    @meetingGp2.destroy

    @meetingGp1Ev1.destroy
    @meetingGp1Ev2.destroy
    @meetingGp1Ev3.destroy
    @meetingGp2Ev1.destroy
    @meetingGp2Ev2.destroy

    @location1.destroy
    @location2.destroy

    @cro.destroy
    @ger.destroy
    @eng.destroy
    @fre.destroy
    @ita.destroy
    @rus.destroy
    @chi.destroy
  end#destroy_user_skill_events_2

  def create_visitor(id)
    User.create!(name: "Visitor#{id}", email: "visitor#{id}@example.com",
      password: "somepassword1")
  end

end#EventSetup

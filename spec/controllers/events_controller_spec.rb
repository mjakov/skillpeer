require 'rails_helper'
require 'pry'

describe EventsController do

  before :each do
    @user = create(:user)
    @user.role = 4
    @user.save

    @facUsrOtr = create(:user)
    @facUsrOtr.role = 4
    @facUsrOtr.save

    @group = build(:group)
    @group.facilitator_id = @user.id
    @group.area_size = 2;
    @group.area_count = 2;
    @group.save

    @othGrp = build(:group)
    @othGrp.facilitator_id = @user.id
    @othGrp.save

    @event = build(:event)
    @event.group_id = @group.id
    @event.save

    @knowhow1 = Knowhow.create(name: "Croatian A1")
    @knowhow2 = Knowhow.create(name: "English A1")

    @evtVisitor = create(:user)
    @evtVisitor2 = create(:user)
    @evtVisitor3 = create(:user)
    @evtVisitor4 = create(:user)

    @evtVisitor.learns(@knowhow1)
    @evtVisitor2.learns(@knowhow1)
    @evtVisitor3.learns(@knowhow2)
    @evtVisitor4.learns(@knowhow2)

    @evtVisitor.teaches(@knowhow2)
    @evtVisitor2.teaches(@knowhow2)
    @evtVisitor3.teaches(@knowhow1)
    @evtVisitor4.teaches(@knowhow1)

    @event.attend(@evtVisitor)
    @event.attend(@evtVisitor2)
    @event.attend(@evtVisitor3)
    @event.attend(@evtVisitor4)

    @visitors = [@evtVisitor, @evtVisitor2, @evtVisitor3, @evtVisitor4]
  end

  after :each do
    @user.destroy
    @facUsrOtr.destroy
    @group.destroy
    @othGrp.destroy
    @event.destroy
    @evtVisitor.destroy
    @evtVisitor2.destroy
    @evtVisitor3.destroy
    @evtVisitor4.destroy
    @knowhow1.destroy
    @knowhow2.destroy
  end

  describe 'access to an event' do

    before :each do
      session[:current_user_id] = @user.id
      @user.role = 4
      @user.save
    end

    context "when owning facilitator logged in" do

      before :each do
        session[:current_user_id] = @user.id
        @user.role = 4
        @user.save
      end

      describe "GET #show" do

        it "renders event #show view" do
          get :show, params: {group_id: @group.id, id: @event.id}
          expect(response).to render_template(:show)
        end

        it "'visitors' variable is set" do
          get :show, params: {group_id: @group.id, id: @event.id}
          expect(assigns(:visitors)).to eq(@visitors)
        end

        context "wrong group_id given" do
          it "redirects to root page" do
            get :show, params: {group_id: @othGrp.id, id: @event.id}
            expect(response).to redirect_to(root_url);
          end
        end

      end

      describe "GET #edit" do
        it "assigns the requested event to @event" do
          get :edit, params: {group_id: @group.id, id: @event.id}
          expect(assigns(:group)).to eq @group
          expect(assigns(:event)).to eq @event
        end

        it "renders the :edit template" do
          get :edit, params: {group_id: @group.id, id: @event.id}
          expect(response).to render_template(:edit)
        end
      end#describe GET #edit

      describe "PATCH #update" do

        context "when wrong event update data" do

          it "does not update the event in the database" do
            oldEndTime = @event.time_end
            newEndTime = @event.time_start - 1.hours
            patch :update, params: { group_id: @group.id, id: @event.id,
              event: attributes_for(:event, time_end: newEndTime) }
            expect(flash[:danger]).not_to eq(nil)
            expect(flash[:success]).to eq(nil)
            expect(response).to render_template(:edit)
            @event.reload
            expect(@event.time_end).to eq(oldEndTime)
          end#it

        end#context wrong data

        context "when correct event update data" do

          it "updates the event in the database" do
            newEndTime = @event.time_end - 1.hours
            patch :update, params: { group_id: @group.id, id: @event.id,
              event: attributes_for(:event, time_end: newEndTime) }
            expect(flash[:danger]).to eq(nil)
            expect(flash[:success]).to_not eq(nil)
            expect(response).to redirect_to(group_path(@group))
            @event.reload
            expect(@event.time_end).to eq(newEndTime)
          end#it

        end#context correct data

      end#describe PATCH #update

      describe "DELETE #destroy" do

        context "when group id matches the event's group" do

          it "deletes the event and redirects to group page" do
            visitorEvtCnt = @evtVisitor.events.count
            expect {
              delete :destroy, params: {group_id: @group.id, id: @event.id}
            }.to change(Event, :count).by(-1)
            @evtVisitor.reload
            expect(@evtVisitor.events.count).to eq(visitorEvtCnt - 1)
            expect(response).to redirect_to(group_path(@group))
            expect(flash[:success]).to_not eq(nil)
          end

        end#context

        context "when group id does not match the event's group" do
          it "does not delete the event and redirects to root url" do

            expect {
              delete :destroy, params: {group_id: @othGrp.id, id: @event.id}
            }.to_not change{Event.count}

            expect(response).to redirect_to(root_url)
            expect(flash[:success]).to eq(nil)
          end
        end#context

      end#describe DELETE #destroy

    end #context owning facilitator logged in

    context "when non owning facilitator logged in" do

      before :each do
        usrOther = create(:user)
        usrOther.role = 4
        usrOther.save
        session[:current_user_id] = usrOther.id
      end

      describe "GET #show" do
        it "he can't view the group" do
          get :show, params: {group_id: @group.id, id: @event.id}
          expect(response).to redirect_to(root_url)
        end
      end

      describe "GET #edit" do
        it "redirects to root url" do
          get :edit, params: {group_id: @group.id, id: @event.id}
          expect(response).to redirect_to(root_url)
        end#it
      end#describe GET #edit

      describe "PATCH #update" do
        it "does not update the event in the database" do
          oldEndTime = @event.time_end
          newEndTime = @event.time_end - 1.hours
          patch :update, params: { group_id: @group.id, id: @event.id,
            event: attributes_for(:event, time_end: newEndTime) }
          expect(response).to redirect_to(root_url)
          @event.reload
          expect(@event.time_end).to eq(oldEndTime)
        end
      end#describe

      describe "DELETE #destroy" do
        context "when group id matches the event's group" do
          it "does not delete the event and redirects to root url" do
            expect {
              delete :destroy, params: {group_id: @group.id, id: @event.id}
            }.to_not change{ Event.count }

            expect(response).to redirect_to(root_url)
            expect(flash[:success]).to eq(nil)
          end
        end#context

        context "when group id does not match the event's group" do
          it "does not delete the event and redirects to root url" do

            expect {
              delete :destroy, params: {group_id: @othGrp.id, id: @event.id}
            }.to_not change{ Event.count }

            expect(response).to redirect_to(root_url)
            expect(flash[:success]).to eq(nil)
          end
        end#context
      end#describe DELETE

    end#context when non owning facilitator logged in

    context "when visitor logged in" do

      before :each do
        @user.role = 2
        @user.save
      end

      describe "GET #edit" do
        it "redirects to root url" do
          get :edit, params: {group_id: @group.id, id: @event.id}
          expect(response).to redirect_to(root_url)
        end#it
      end#describe GET #edit

      describe "PATCH #update" do
        it "redirects to root url" do
          oldEndTime = @event.time_end
          newEndTime = @event.time_end - 1.hours
          patch :update, params: { group_id: @group.id, id: @event.id,
            event: attributes_for(:event, time_end: newEndTime) }
          expect(response).to redirect_to(root_url)
          @event.reload
          expect(@event.time_end).to eq(oldEndTime)
        end#it
      end#describe PATCH #update

      describe "GET #show" do

        before :each do
          tom = User.create(name: "Tom Sawyer", email: "tom@example.com", password: "password1")
          mark = User.create(name: "Mark Twain", email: "mark@example.com", password: "password1")

          cro = Knowhow.create(name: "Croatian")
          eng = Knowhow.create(name: "English")
          ger = Knowhow.create(name: "German")

          tom.learns(cro)
          tom.teaches(eng)
          tom.learns(ger)

          mark.learns(cro)
          mark.teaches(eng)
          mark.teaches(ger)

          @event.attend(tom)
          @event.attend(mark)
        end

        it "redirects to event show view" do
          get :show, params: { group_id: @group.id, id: @event.id }
          expect(response).to render_template(:show)
        end

        it "sets the skill statistic variables" do
          get :show, params: { group_id: @group.id, id: @event.id }
          expect(assigns(:group)).to eq(@group)
          expect(assigns(:event)).to eq(@event)

          expect(assigns(:learning_knowhows)).not_to eq(nil)
          expect(assigns(:learning_knowhows).empty?).to eq(false)

          expect(assigns(:teaching_knowhows)).not_to eq(nil)
          expect(assigns(:teaching_knowhows).empty?).to eq(false)
        end

        it "sets the visitors variable to nil" do
          visitors = []
          get :show, params: {group_id: @group.id, id: @event.id}
          expect(assigns(:visitors)).to eq(visitors)
        end

        context "wrong group id given" do
          it "redirects to root" do
            get :show, params: { group_id: @othGrp.id, id: @event.id }
            expect(response).to redirect_to(root_url)
          end
        end

      end#describe GET #show

      describe "DELETE #destroy" do

        context "when group id matches the event's group" do
          it "does not delete the event and redirects to root url" do
            expect {
              delete :destroy, params: {group_id: @group.id, id: @event.id}
            }.to_not change{ Event.count }

            expect(response).to redirect_to(root_url)
            expect(flash[:success]).to eq(nil)
          end
        end#context

        context "when group id does not match the event's group" do
          it "does not delete the event and redirects to root url" do

            expect {
              delete :destroy, params: {group_id: @othGrp.id, id: @event.id}
            }.to_not change{ Event.count }

            expect(response).to redirect_to(root_url)
            expect(flash[:success]).to eq(nil)
          end
        end#context

      end#describe DELETE

    end#context when visitor logged in

    context "when user not logged in" do

      before :each do
        session[:current_user_id] = nil
      end

      describe "GET #edit" do
        it "redirects to root url" do
          get :edit, params: {group_id: @group.id, id: @event.id}
          expect(response).to redirect_to(root_url)
        end#it
      end#describe GET #edit

      describe "PATCH #update" do
        it "redirects to root url" do
          oldEndTime = @event.time_end
          newEndTime = @event.time_end - 1.hours
          patch :update, params: { group_id: @group.id, id: @event.id,
            event: attributes_for(:event, time_end: newEndTime) }
          expect(response).to redirect_to(root_url)
          @event.reload
          expect(@event.time_end).to eq(oldEndTime)
        end#it
      end#describe

      describe "DELETE #destroy" do

        context "when group id matches the event's group" do
          it "does not delete the event and redirects to root url" do
            expect {
              delete :destroy, params: {group_id: @group.id, id: @event.id}
            }.to_not change{ Event.count }

            expect(response).to redirect_to(root_url)
            expect(flash[:success]).to eq(nil)
          end
        end#context

        context "when group id does not match the event's group" do
          it "does not delete the event and redirects to root url" do

            expect {
              delete :destroy, params: {group_id: @othGrp.id, id: @event.id}
            }.to_not change{ Event.count }

            expect(response).to redirect_to(root_url)
            expect(flash[:success]).to eq(nil)
          end
        end#context

      end#describe delete

    end#context user not logged in

    describe "POST #attend" do
      before :each do
        @visitorx = build(:user)
        @visitorx.role = 2
        @visitorx.validate
        expect(@visitorx.valid?).to eq(true)
        @visitorx.save
        session[:current_user_id] = @visitorx.id
      end

      after :each do
        @visitorx.events.delete_all
        @visitorx.destroy
      end

      context "when logged in visitor" do
        context "when not attending this event" do
          context "when enough places" do
            it "can attend event" do
              unless @event.users.where(id: @visitorx.id).first.nil?
                @event.users.where(id: @visitorx.id).first.delete
              end
              @event.reload
              @visitorx.reload
              expect {
                post :attend, params: { group_id: @group.id, id: @event.id }
              }.to change{ @event.users.count }.by(1)
              @event.users.find(@visitorx.id).delete
            end
            context "when wrong group id event id pair" do
              it "cannot attend event" do
                expect {
                  post :attend, params: { group_id: @othGrp.id, id: @event.id }
                }.to_not change{ @event.users.count }
                expect(response).to redirect_to(root_url)
              end
            end
          end
          context "when not enough places" do
            it "cannot attend event" do
              @group.max_places = 0
              @group.save
              expect {
                post :attend, params: { group_id: @group.id, id: @event.id }
              }.to_not change{ @event.users.count }
              expect(response).to redirect_to(root_url)
            end
          end #context "when not enough places"
        end #context "when not attending this event"
        context "when attending this event" do
          it "cannot register to attend same event twice" do
            unless @event.users.where(id: @visitorx.id).first.nil?
              @event.users.where(id: @visitorx.id).first.delete
            end
            expect {
              post :attend, params: { group_id: @group.id, id: @event.id }
            }.to change{ @event.users.count }.by(1)
            expect {
              post :attend, params: { group_id: @group.id, id: @event.id }
            }.to change{ @event.users.count }.by(0)
            @event.users.find(@visitorx.id).delete
            expect(response).to redirect_to(root_url)
          end
        end
      end#context logged in visitor
      context "when not logged in user" do
        it "cannot attend event" do
          session[:current_user_id] = nil
          expect {
            post :attend, params: { group_id: @group.id, id: @event.id }
          }.to_not change{ @event.users.count }
          expect(response).to redirect_to(root_url)
        end
      end#not logged in user
    end#describe POST attend

    describe "POST #unattend" do
      context "when logged in as visitor" do

        before :each do
          @visitor = build(:user)
          @visitor.role = 2
          @visitor.save
          session[:current_user_id] = @visitor.id
          puts "before each when logged in as visitor"
        end

        after :each do
          @visitor.destroy
          puts "after each when logged in as visitor"
        end

        context "when attending this event" do

          before :each do
            @visitor.reload
            @event.attend(@visitor)
          end

          after :each do
            @event.users.delete(@visitor)
          end

          context "group id and event id match" do
            it "can unattend" do
              expect {
                post :unattend, params: { group_id: @group.id, id: @event.id }
              }.to change{ @event.users.count }.by(-1)
            end
          end#context

          context "group id and event id match" do
            it "does not delete the visitor from the users table" do
              expect {
                post :unattend, params: { group_id: @group.id, id: @event.id }
              }.not_to change{ User.all.count }
            end
          end#context

          context "group id and event id don't match" do
            it "cannot unattend" do
              expect {
                post :unattend, params: { group_id: @othGrp.id, id: @event.id }
              }.to_not change{ @event.users.count }
            end
          end#context

          context "when not attending this event" do

            before :each do
              @othEvt = build(:event)
              @othEvt.group_id = @othGrp.id
              @othEvt.save!
            end

            it "can not unattend" do
              expect {
                post :unattend, params: { group_id: @group.id, id: @othEvt.id }
              }.to_not change{ @othEvt.users.count }
              expect(response).to redirect_to(root_url)
            end
          end

        end#context attending this event

      end#context logged in visitor

      context "when not logged in user" do
        it "can not unattend any event" do
          expect {
            post :unattend, params: { group_id: @group.id, id: @event.id }
          }.to_not change{ @event.users.count }
          expect(response).to redirect_to(root_url)
        end
      end
    end#describe POST unattend

  end#describe event access

  describe "Event #agenda" do
    context "not logged_in user" do
      it "creates a new agenda" do
        session[:current_user_id] = nil
        post :agenda, params: {group_id: @group.id, id: @event.id}
        expect(assigns(:agenda)).to eql(nil)
        expect(response).to redirect_to(root_url)
      end
    end
    context "logged_in user" do
      it "creates a new agenda" do
        session[:current_user_id] = @evtVisitor.id
        post :agenda, params: {group_id: @group.id, id: @event.id}
        expect(assigns(:agenda)).to eql(nil)
        expect(response).to redirect_to(root_url)
      end
    end
    context "non-owning facilitator" do
      it "creates a new agenda" do
        session[:current_user_id] = @facUsrOtr.id
        post :agenda, params: {group_id: @group.id, id: @event.id}
        expect(assigns(:agenda)).to eql(nil)
        expect(response).to redirect_to(root_url)
      end
      it "wants to access event page" do
        session[:current_user_id] = @facUsrOtr.id
        get :show, params: {group_id: @group.id, id: @event.id}
        expect(response).to redirect_to(root_url)
      end

    end
    context "owning facilitator" do
      it "creates a new agenda" do
        session[:current_user_id] = @user.id
        post :agenda, params: {group_id: @group.id, id: @event.id}
        expect(assigns(:agenda)).not_to eql(nil)
        expect(assigns(:agenda)).not_to eql([])
        expect(assigns(:agenda).length).to be >= 1
        expect(response).to render_template :agenda
      end
    end#context
    context "owning facilitator and wrong group/event pair" do
      it "creates a new agenda" do
        session[:current_user_id] = @user.id
        post :agenda, params: {group_id: @othGrp.id, id: @event.id}
        expect(assigns(:agenda)).to eql(nil)
        expect(response).to redirect_to(root_url)
      end
    end#context
  end#describe "Event #agenda"

  describe "Event Visitors-List" do
    context "Owning facilitator can access visitors list" do
      it "Redirects to guests view" do
        session[:current_user_id] = @user.id
        get :visitors, params: {group_id: @group.id, id: @event.id}
        expect(response).to render_template(:visitors)
        session[:current_user_id] = nil
      end
      it "Sets the visitors variable" do
        session[:current_user_id] = @user.id
        get :visitors, params: {group_id: @group.id, id: @event.id}
        expect(assigns(:visitors)).to eq(@event.users)
        session[:current_user_id] = nil
      end
    end#context

    context "owning facilitator and wrong group/event pair" do
      it "can't access visitors list" do
        session[:current_user_id] = @user.id
        post :visitors, params: {group_id: @othGrp.id, id: @event.id}
        expect(assigns(:visitors)).to eql(nil)
        expect(response).to redirect_to(root_url)
      end
    end#context

    context "Non-owning facilitator" do
      it "cannot access visitor list" do
        session[:current_user_id] = @facUsrOtr.id
        get :visitors, params: {group_id: @group.id, id: @event.id}
        expect(response).to redirect_to(root_url)
        session[:current_user_id] = nil
      end
    end
    context "Visitor" do
      it "cannot access visitor list" do
        session[:current_user_id] = @evtVisitor.id
        get :visitors, params: {group_id: @group.id, id: @event.id}
        expect(response).to redirect_to(root_url)
        session[:current_user_id] = nil
      end
    end
    context "Not logged-in user" do
      it "cannot access visitor list" do
        session[:current_user_id] = nil
        get :visitors, params: {group_id: @group.id, id: @event.id}
        expect(response).to redirect_to(root_url)
      end
    end
  end#describe "Event Visitors-List"

end#describe EventsController

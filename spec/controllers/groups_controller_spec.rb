require 'rails_helper'

RSpec.describe GroupsController, type: :controller do

	describe "POST #create" do

		before :each do
			@fac1 = create(:user)
			@fac1.make_facilitator()
			@fac1.save

			@grp1 = Group.new(name: "Language Conversation Table", max_places: 30,
					description: "This is a language learning community.", area_size: 5,
					area_count: 3)

			@loc1 = Location.new(name: "Pivnica Pinta", country: "Croatia", postnr: 10000,
				city: "Zagreb", street: "Radiceva", number: "3a")

		end

		after :each do
			@fac1.destroy
			@grp1.destroy
			@loc1.destroy
		end

		context "Location does not already exist" do

			it "Creates new location" do
				login_as(@fac1)

				expect {
					post :create, params: { group: { name: @grp1.name, max_places: @grp1.max_places,
						description: @grp1.description, area_size: @grp1.area_size,
						area_count: @grp1.area_count, location: { name: @loc1.name, country: @loc1.country,
						postnr: @loc1.postnr, city: @loc1.city, street: @loc1.street, number: @loc1.number } } }
				}.to change(Location.all, :count).by(1)

				logout()
			end #it

			it "Creates new group" do
				login_as(@fac1)

				expect {
					post :create, params: { group: { name: @grp1.name, max_places: @grp1.max_places,
						description: @grp1.description, area_size: @grp1.area_size,
						area_count: @grp1.area_count, location: { name: @loc1.name, country: @loc1.country,
						postnr: @loc1.postnr, city: @loc1.city, street: @loc1.street, number: @loc1.number } } }
				}.to change(Group.all, :count).by(1)

				logout()
			end #it

		end #context

		context "Location already exists" do

			it "Just assigns old location" do
				@loc1.save
				login_as(@fac1)

				expect {
					post :create, params: { group: { name: @grp1.name, max_places: @grp1.max_places,
						description: @grp1.description, area_size: @grp1.area_size,
						area_count: @grp1.area_count, location: { name: @loc1.name, country: @loc1.country,
						postnr: @loc1.postnr, city: @loc1.city, street: @loc1.street, number: @loc1.number } } }
				}.not_to change(Location.all, :count)

				@loc1.reload

				expect(assigns(:group)).to eq(@loc1.groups.first)

				logout()
			end #it

			it "Creates new group" do
				@loc1.save
				login_as(@fac1)

				expect {
					post :create, params: { group: { name: @grp1.name, max_places: @grp1.max_places,
						description: @grp1.description, area_size: @grp1.area_size,
						area_count: @grp1.area_count, location: { name: @loc1.name, country: @loc1.country,
						postnr: @loc1.postnr, city: @loc1.city, street: @loc1.street, number: @loc1.number } } }
				}.to change(Group.all, :count).by(1)

				@loc1.reload
				expect(assigns(:group)).to eq(@loc1.groups.first)

				logout()
			end #it

		end #context

	end #describe

	describe "PATCH #update" do
		context "Uses another old location" do

			before :each do
				@fac1 = create(:user)
				@fac1.make_facilitator()
				@fac1.save
				@grp1 = Group.new(name: "Language Conversation Table", max_places: 30,
						description: "This is a language learning community.", area_size: 5,
						area_count: 3)
				@grp1.facilitator = @fac1
				@grp1.save
				@grpOtr = Group.new(name: "New Language Conversation Table", max_places: 40,
						description: "This is a new language learning community.", area_size: 6,
						area_count: 4)
				@loc1 = Location.create!(name: "Pivnica Pinta", country: "Croatia", postnr: 10000,
					city: "Zagreb", street: "Radiceva", number: "3a")
				@locDef = Location.find_by(name: "Default")
			end

			after :each do
				@fac1.destroy
				@grp1.destroy
				@loc1.destroy
			end

			it "Does not create a new location" do
				login_as(@fac1)
				expect {
					patch :update, params: { id: @grp1.id, group: { name: @grp1.name, max_places: @grp1.max_places,
						description: @grp1.description, area_size: @grp1.area_size,
						area_count: @grp1.area_count, location: { name: @loc1.name, country: @loc1.country,
						postnr: @loc1.postnr, city: @loc1.city, street: @loc1.street, number: @loc1.number } } }
				}.not_to change(Location.all, :count)
				logout()
			end # example
			it "Updates the group with location data" do
				login_as(@fac1)
				patch :update, params: { id: @grp1.id, group: { name: @grp1.name, max_places: @grp1.max_places,
					description: @grp1.description, area_size: @grp1.area_size,
					area_count: @grp1.area_count, location: { name: @loc1.name, country: @loc1.country,
					postnr: @loc1.postnr, city: @loc1.city, street: @loc1.street, number: @loc1.number } } }
				@grp1.reload
				expect(@grp1.location).to eq(@loc1)
				logout()
			end # example
			it "Updates the group with new group data" do
				login_as(@fac1)
				patch :update, params: { id: @grp1.id, group: { name: @grpOtr.name, max_places: @grpOtr.max_places,
					description: @grpOtr.description, area_size: @grpOtr.area_size,
					area_count: @grpOtr.area_count, location: { name: @loc1.name, country: @loc1.country,
					postnr: @loc1.postnr, city: @loc1.city, street: @loc1.street, number: @loc1.number } } }
				@grp1.reload
				expect(@grp1.attributes.slice(
					"name", "max_places", "description", "area_count", "area_size")).to eq(
						@grpOtr.attributes.slice(
						"name", "max_places", "description", "area_count", "area_size"))
				logout()
			end # example
		end #context

		context "Uses new location" do

			before :each do
				@fac1 = create(:user)
				@fac1.make_facilitator()
				@fac1.save
				@grp1 = Group.new(name: "Language Conversation Table", max_places: 30,
						description: "This is a language learning community.", area_size: 5,
						area_count: 3)
				@grp1.facilitator = @fac1
				@grp1.save
				@loc1 = Location.new(name: "Pivnica Pinta", country: "Croatia", postnr: 10000,
					city: "Zagreb", street: "Radiceva", number: "3a")
				@locDef = Location.find_by(name: "Default")
			end

			after :each do
				@fac1.destroy
				@grp1.destroy
				@loc1.destroy
			end

			it "Creates new location and updates the group with location data" do
				login_as(@fac1)
				expect {
					patch :update, params: { id: @grp1.id, group: { name: @grp1.name, max_places: @grp1.max_places,
						description: @grp1.description, area_size: @grp1.area_size,
						area_count: @grp1.area_count, location: { name: @loc1.name, country: @loc1.country,
						postnr: @loc1.postnr, city: @loc1.city, street: @loc1.street, number: @loc1.number } } }
				}.to change(Location.all, :count).by(1)
				@grp1.reload
				expect(@grp1.location.attributes.
					slice("name", "country", "postnr", "city", "street", "number")).to eq(
					@loc1.attributes.slice("name", "country", "postnr", "city", "street", "number"))
				logout()
			end # example
		end # context

		context "Uses invalid new location" do

			before :each do
				@fac1 = create(:user)
				@fac1.make_facilitator()
				@fac1.save
				@grp1 = Group.new(name: "Language Conversation Table", max_places: 30,
						description: "This is a language learning community.", area_size: 5,
						area_count: 3)
				@grp1.facilitator = @fac1
				@grp1.save
				@loc1 = Location.new(name: "Piv", country: "Cr", postnr: "a",
					city: "Za", street: "Ra", number: "3a")
				@locDef = Location.find_by(name: "Default")
			end

			after :each do
				@fac1.destroy
				@grp1.destroy
				@loc1.destroy
			end

			it "Does not update group" do
				login_as(@fac1)
				expect {
					patch :update, params: { id: @grp1.id, group: { name: @grp1.name, max_places: @grp1.max_places,
						description: @grp1.description, area_size: @grp1.area_size,
						area_count: @grp1.area_count, location: { name: @loc1.name, country: @loc1.country,
						postnr: @loc1.postnr, city: @loc1.city, street: @loc1.street, number: @loc1.number } } }
				}.not_to change(Location.all, :count)
				logout()
			end # example
		end # context

	end # describe

	describe "GET #index" do

		context "Non-registered user" do

			before :each do
				@locations = []
				@groups = []
				@fac = build(:user)
				@fac.make_facilitator
				@fac.save
				for i in 0..4
					@locations[i] = build(:location)
				end

				@locations[0].country = "Brazil"
				@locations[0].city = "Sao Paolo"
				@locations[1].country = "Argentina"
				@locations[1].city = "Buenos Aires"
				@locations[2].country = "Brazil"
				@locations[2].city = "Rio de Janeiro"
				@locations[3].country = "Chile"
				@locations[3].city = "Santiago"
				@locations[4].country = "Argentina"
				for i in 0..4
					@locations[i].save
				end

				# Groups 8 and 9 would have the default location if i was in 0..9
				for i in 0..7
					@groups[i] = build(:group)
					@groups[i].facilitator = @fac
				end
				for i in 0..3
					@groups[2*i].location = @locations[i]
					@groups[2*i + 1].location = @locations[i]
				end
				for grp in @groups
					grp.save
				end
			end #before

			after :each do
				for loc in @locations
					loc.destroy
				end
				for grp in @groups
					grp.destroy
				end
				@fac.destroy
			end #after

			it "Shows all groups" do
				get :index
				expect(response).to render_template(:index)
				expect(assigns(:groups_hash).keys.count).to eq(3) # three countries used
				expect( (assigns(:groups_hash)["Brazil"]).keys.count ).to eq(2) # two cities
				expect( (assigns(:groups_hash)["Argentina"]).keys.count ).to eq(1) # one city used
				expect( (assigns(:groups_hash)["Chile"]).keys.count ).to eq(1) # one city
			end

		end #context

	end #describe

end #RSpec

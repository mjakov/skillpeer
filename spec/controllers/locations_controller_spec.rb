require 'rails_helper'

RSpec.describe LocationsController, type: :controller do

  describe "GET #index" do

    before :each do 
      @testLocs = []
      for i in 1..10
        @testLocs << create(:location)
      end      
    end 

    after :each do 
      for loc in @testLocs 
        loc.destroy
      end
    end

    it "returns http success" do
      get :index, params: { term: "Pinta"}
      expect(response).to have_http_status(:success)
    end #example

    it "returns a JSON list of locations" do
      get :index, params: { term: @testLocs[0].name[1..5] }
      loc_names = Location.order(:name).where("name like ?", "%#{@testLocs[0].name[1..5]}%").
        map { |loc| loc.name+", "+loc.street+", "+loc.number+", "+loc.city+", "+loc.postnr+", "+loc.country }
      loc_names_resp = JSON.parse(response.body)
      expect(loc_names_resp).to eq(loc_names)
    end #example
  end #describe

end #RSpec.describe

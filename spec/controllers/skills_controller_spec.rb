require 'rails_helper'

RSpec.describe SkillsController, type: :controller do

  before :each do
    @visitor1 = create(:user)
    @visitor2 = create(:user)
    @knowhow1 = Knowhow.create(name: "Croatian A")
    @knowhow2 = Knowhow.create(name: "English A")
    @knowhow3 = Knowhow.create(name: "German A")
  end

  describe "GET #new:" do

    context "when un-logged-in user tries to add a skill to someone," do
      it "redirects him to the root url" do
        get :new, params: { user_id: @visitor1.id}
        expect(response).to redirect_to(root_url)
      end
    end#context

    context "when logged_in user tries to add skills to himself" do
      it "shows him his new skill template" do
        session[:current_user_id] = @visitor1.id
        get :new, params: { "user_id" =>  @visitor1.id }
        expect(response).to render_template :new
      end
    end#context

    context "when logged_in user tries to add skills to other user" do
      it "redirects to root url" do
        get :new, params: { user_id: @visitor1.id}, session: {current_user_id: @visitor2.id}
        expect(response).to redirect_to(root_url)
      end
    end#context

  end#describe GET #new

  describe "POST #create" do

    context "not logged in user tries to create a skill for other user" do
      it "is redirected to root page" do
        expect {
          post :create, params: { user_id: @visitor1.id, skill: {knowhow_id: 1, teaching: false} }
        }.not_to change(@visitor1.skills, :count)
        expect(response).to redirect_to(root_url)
      end
    end#context

    context "logged in user tries to create skill for other user" do
      it "is redirected to root page" do
        login_as(@visitor1)
        expect {
          post :create, params: { user_id: @visitor2.id, skill: {knowhow_id: 2, teaching: false} }
        }.not_to change(@visitor2.skills, :count)
        expect(response).to redirect_to(root_url)
        logout
      end
    end#context

    context "logged in user tries to create skill for himself" do
      it "adds the skill to the user and it routes to his profile page" do
        login_as(@visitor1)
        expect(@visitor1.skills.find_by(knowhow_id: 2)).to eq(nil)
        expect {
          post :create, params: { user_id: @visitor1.id, skill: {knowhow_id: 2, teaching: true} }
        }.to change(@visitor1.skills, :count).by(1)
        skill = @visitor1.skills.find_by(knowhow_id: 2)
        expect(skill).not_to eq(nil)
        if skill
          expect(skill.knowhow_id).to eq(2)
          expect(skill.teaching).to eq(true)
        end
        expect(response).to redirect_to(user_path(@visitor1))
        expect(flash[:success]).not_to eq(nil)
        logout
      end
    end#context

    context "logged in user tries to create the same skill for himself twice" do
      it "adds the skill to the user and it routes to his profile page" do
        login_as(@visitor1)
        expect(@visitor1.skills.find_by(knowhow_id: 2)).to eq(nil)
        expect {
          post :create, params: { user_id: @visitor1.id, skill: {knowhow_id: 2, teaching: true} }
        }.to change(@visitor1.skills, :count).by(1)
        expect {
          post :create, params: { user_id: @visitor1.id, skill: {knowhow_id: 2, teaching: true} }
        }.not_to change(@visitor1.skills, :count)
        expect(flash[:error]).not_to eq(nil)
        logout
      end
    end#context

    context "logged in user tries to create a skill with non-supported knowhow" do
      it "does not add the skill and it routes to the root page" do
        login_as(@visitor1)
        expect {
          post :create, params: { user_id: @visitor1.id, skill: {knowhow_id: 99999, teaching: false} }
        }.not_to change(@visitor1.skills, :count)
        expect(response).to render_template(:new)
        logout
      end
    end#context

  end#describe POST #create

  describe "DELETE #destroy" do

    context "logged in user" do

      before(:each) do
        expect {
          skill = @visitor1.skills.create(knowhow_id: @knowhow1.id)
          skill.save
        }.to change(@visitor1.skills, :count).by(1)
        expect {
          skill = @visitor2.skills.create(knowhow_id: @knowhow2.id)
          skill.save
        }.to change(@visitor2.skills, :count).by(1)

        login_as(@visitor1)
      end

      after(:each) do
        logout()
      end

      context "tries to delete one of his skills" do
        it "deletes that skill" do
          new_skill = @visitor1.skills.first
          expect {
            delete :destroy, params: { user_id: @visitor1.id, id: new_skill.id }
          }.to change(@visitor1.skills, :count).by(-1)
          expect(response).to redirect_to user_path(@visitor1)
          expect(flash[:success]).not_to eq(nil)
        end
      end#context

      context "tries to delete his own non-existing skill" do

        it "nothing is deleted" do
          skill_id = 9797978
          expect {
            delete :destroy, params: { user_id: @visitor1.id, id: skill_id }
          }.not_to change(@visitor1.skills, :count)
        end

        it "he is redirected to root" do
          skill_id = 9797978
          delete :destroy, params: { user_id: @visitor1.id, id: skill_id }
          expect(response).to redirect_to(root_path)
        end

      end#context

      context "tries to delete someone else's skill" do
        it "does not delete that skill" do
          anothers_skill = @visitor2.skills.first
          expect {
            delete :destroy, params: { user_id: @visitor2.id, id: anothers_skill.id }
          }.not_to change(@visitor1.skills, :count)
        end
        it "is redirected to root page" do
          anothers_skill = @visitor2.skills.first
          delete :destroy, params: { user_id: @visitor2.id, id: anothers_skill.id }
          expect(response).to redirect_to(root_path)
        end
      end#context

    end#context logged in user

    context "not logged in user tries to delete someone else's skill" do

      before(:each) do
        expect {
          skill = @visitor1.skills.create(knowhow_id: @knowhow1.id)
          skill.save
        }.to change(@visitor1.skills, :count).by(1)
        expect {
          skill = @visitor2.skills.create(knowhow_id: @knowhow2.id)
          skill.save
        }.to change(@visitor2.skills, :count).by(1)
      end

      it "does not delete that skill" do
        skill = @visitor1.skills.first
        expect {
          delete :destroy, params: { user_id: @visitor1.id, id: skill.id }
        }.not_to change(@visitor1.skills, :count)
      end

      it "is redirected to root page" do
        skill = @visitor1.skills.first
        delete :destroy, params: { user_id: @visitor1.id, id: skill.id }
        expect(response).to redirect_to(root_path)
      end
    end#context

  end#describe DELETE #destroy


end#rspec

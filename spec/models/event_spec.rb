require 'rails_helper'

RSpec.describe Event, type: :model do

  def setup_user_skill_events_1
    @thomas_jefferson = User.create!(name: "Thomas Jefferson", email: "thomas@example.com",
        password: "somepassword1")
    @tom_sawyer = User.create!(name: "Tom Sawyer", email: "tomsawyer@example.com",
        password: "somepassword1", role: User.roles([:VISITOR, :FACILITATOR]) )
    @mike_tyson = User.create!(name: "Mike Tyson", email: "miketyson@example.com",
        password: "somepassword1", role: User.roles([:VISITOR, :FACILITATOR]) )

    @meetingGp1 = Group.create!(name: "Language Conversation Table", max_places: 30,
      description: "We meet Sundays at 5 P.M.", facilitator_id: @tom_sawyer.id,
      area_size: 6, area_count: 5)

    @meetingGp1Ev1 = Event.create!(group_id: @meetingGp1.id,
      time_start: DateTime.parse("2099-11-02 17:00"), time_end: DateTime.parse("2099-11-02 20:00"))
    @meetingGp1Ev2 = Event.create!(group_id: @meetingGp1.id,
        time_start: DateTime.parse("2099-11-05 17:00"), time_end: DateTime.parse("2099-11-05 20:00"))

    @cro = Knowhow.create(name: "Croatian")
    @ger = Knowhow.create(name: "German")
    @eng = Knowhow.create(name: "English")
    @fre = Knowhow.create(name: "French")
    @ita = Knowhow.create(name: "Italian")
    @rus = Knowhow.create(name: "Russian")
    @chi = Knowhow.create(name: "Chinese")

    @tom_sawyer.skills.create!(knowhow_id: @cro.id, teaching: true)
    @tom_sawyer.skills.create!(knowhow_id: @ger.id)
    @mike_tyson.skills.create!(knowhow_id: @cro.id, teaching: true)
    @mike_tyson.skills.create!(knowhow_id: @ger.id)
    @thomas_jefferson.skills.create!(knowhow_id: @ger.id, teaching: true)

    @meetingGp1Ev1.attend(@tom_sawyer)
    @meetingGp1Ev1.attend(@thomas_jefferson)
    @meetingGp1Ev2.attend(@mike_tyson)
  end

  def destroy_user_skill_events_1
    @meetingGp1Ev1.destroy
    @meetingGp1Ev2.destroy
    @meetingsGp1.destroy
    @thomas_jefferson.destroy
    @tom_sawyer.destroy
    @mike_tyson.destroy
    @cro.destroy
    @ger.destroy
    @eng.destroy
    @fre.destroy
    @ita.destroy
    @rus.destroy
    @chi.destroy
  end

  context "users with skills visiting an event 1" do

    before :each do
      setup_user_skill_events_1
    end

    # after :each do
    #   destroy_user_skill_events_1
    # end

    it "gives proper teaching statistic of knowhows for event 1" do
      stat = @meetingGp1Ev1.teaching
      expect(stat["Croatian"]).to eq(1)
      expect(stat["German"]).to eq(1)
    end

    it "gives proper learning statistic of knowhows for event 1" do
      stat = @meetingGp1Ev1.learning
      expect(stat["Croatian"]).to eq(nil)
      expect(stat["German"]).to eq(1)
    end

    it "gives proper teaching statistic of knowhows for event 2" do
      stat = @meetingGp1Ev2.teaching
      expect(stat["Croatian"]).to eq(1)
      expect(stat["German"]).to eq(nil)
    end

    it "gives proper learning statistic of knowhows for event 2" do
      stat = @meetingGp1Ev2.learning
      expect(stat["Croatian"]).to eq(nil)
      expect(stat["German"]).to eq(1)
    end

  end#context

  context "testing skills statistic for event set 2" do

    before :each do
      setup_user_skill_events_2
    end

    after :each do
      destroy_user_skill_events_2
    end

    it "should give proper teaching skill statistics for group1 event1" do
      stat = @meetingGp1Ev1.teaching
      expect(stat["English"]).to eq(3)
      expect(stat["Croatian"]).to eq(1)
      expect(stat["French"]).to eq(1)
    end

    it "should give proper teaching skill statistics for group1 event2" do
      stat = @meetingGp1Ev2.teaching
      expect(stat["English"]).to eq(3)
      expect(stat["Croatian"]).to eq(nil)
      expect(stat["French"]).to eq(1)
    end

    it "should give proper learning skill statistics for group1 event1" do
      stat = @meetingGp1Ev1.learning
      expect(stat["French"]).to eq(1)
      expect(stat["English"]).to eq(1)
      expect(stat["German"]).to eq(4)
      expect(stat["Croatian"]).to eq(1)
    end

    it "should give proper learning skill statistics for group1 event2" do
      stat = @meetingGp1Ev2.learning
      expect(stat["French"]).to eq(1)
      expect(stat["English"]).to eq(nil)
      expect(stat["German"]).to eq(3)
      expect(stat["Croatian"]).to eq(1)
    end

  end

end

require 'rails_helper'

RSpec.describe Knowhow, type: :model do

  it "is invalid with a duplicate name" do
    expect {
      Knowhow.create(name: "Spanish B")
    }.to change { Knowhow.count }.by(1)

    expect {
      begin
        Knowhow.create(name: "Spanish B")
      rescue => ex
        #puts "Testing exception: #{ex.class} #{ex.message}"
      end
    }.to_not change { Knowhow.count }
  end

  it "is invalid if name not present" do
    knowhow1 = Knowhow.new(name: "")
    expect(knowhow1.valid?).to eq(false)
  end

  it "is invalid if name is a duplicate" do
    knowhow1 = Knowhow.create(name: "Spanish B")
    expect(knowhow1.valid?).to eq(true)
    knowhow2 = Knowhow.new(name: "Spanish B")
    expect(knowhow2.valid?).to eq(false)
  end

  it "is invalid if name is too long" do
    knowhow1 = Knowhow.create(name: "Spanish B")
    expect(knowhow1.valid?).to eq(true)
    namestr = ""
    31.times{ namestr += "a"}
    knowhow2 = Knowhow.new(name: namestr)
    expect(knowhow2.valid?).to eq(false)
  end

end#describe Knowhow

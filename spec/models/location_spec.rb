require 'rails_helper'

RSpec.describe Location, type: :model do

  before :each do
    @locA = build(:location)
    @loc1 = Location.new(
      name: "Pivnica Pinta", country: "Croatia", postnr: "10000",
      city: "Zagreb", street: "Radiceva", number: "3a")

    @loc1.save
  end

  after :each do
    @loc1.destroy
    @locA.destroy
  end

  it "initializes postal number properly" do 
    postNrStr = "HR-10000"
    newLoc = Location.new(@locA.attributes)
    newLoc.postnr = postNrStr
    expect(newLoc.postnr).to eq(postNrStr)
  end

  it "initializes properly" do
    expect(@loc1.name).to eq("Pivnica Pinta")
    expect(@loc1.country).to eq("Croatia")
    expect(@loc1.postnr).to eq("10000")
    expect(@loc1.city).to eq("Zagreb")
    expect(@loc1.street).to eq("Radiceva")
    expect(@loc1.number).to eq("3a")
    expect(@loc1.valid?).to eq(true)
  end

  it "needs to have a proper name" do
    @loc1.name = "ABC"
    expect(@loc1.valid?).to eq(false)
    @loc1.name = nil
    expect(@loc1.valid?).to eq(false)

  end

  it "needs to have a proper country" do
    @loc1.country = nil
    expect(@loc1.valid?).to eq(false)
    @loc1.country = "USA"
    expect(@loc1.valid?).to eq(true)
    @loc1.country = "UK"
    expect(@loc1.valid?).to eq(false)
  end

  it "needs to have a proper post number" do
    @loc1.postnr = nil
    expect(@loc1.valid?).to eq(false)
    @loc1.postnr = 10
    expect(@loc1.valid?).to eq(false)
  end

  it "needs to have a proper city" do
    @loc1.city = nil
    expect(@loc1.valid?).to eq(false)
    @loc1.city = "AB"
    expect(@loc1.valid?).to eq(false)
  end

  it "needs to have a proper street" do
    @loc1.street = nil
    expect(@loc1.valid?).to eq(false)
    @loc1.street = "ABC"
    expect(@loc1.valid?).to eq(false)
  end

  it "needs to have a proper street number" do
    @loc1.number = nil
    expect(@loc1.valid?).to eq(false)
  end

  it "has unique name with postnr and country" do
    loc2 = Location.new(
      name: "Pivnica Pinta", country: "Croatia", postnr: 10000,
      city: "Zagreb", street: "Radićeva", number: "3a")
    expect(loc2.valid?).to eq(false)
  end

end

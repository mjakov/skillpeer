require 'rails_helper'
require 'agenda/manager'

include Agenda

RSpec.describe Manager, type: :model do

  def setup_event_visitors
    @visitors = []
    for i in (1..10)
      vis = create_visitor(i)
      @visitors << vis
    end

    @cro = Knowhow.create!(name: "Croatian9")
    @eng = Knowhow.create!(name: "English9")
    @ita = Knowhow.create!(name: "Italian9")
    @ger = Knowhow.create!(name: "German9")
    @spa = Knowhow.create!(name: "Spanish9")
    @ara = Knowhow.create!(name: "Arabic9")
    @swe = Knowhow.create!(name: "Swedish9")
    @fre = Knowhow.create!(name: "French9")
    @rus = Knowhow.create!(name: "Russian9")
    @pol = Knowhow.create!(name: "Polish9")
    @chi = Knowhow.create!(name: "Chinese9")

    @knowhows = [@cro, @eng, @ita, @ger, @spa, @ara, @swe, @fre, @rus, @pol, @chi]


    for i in (0..3)
      @visitors[i].teaches(@cro) # feasible
      @visitors[i].learns(@eng)  # feasible
      @visitors[i].learns(@fre)
      @visitors[i].learns(@ita)
    end
    for i in (4..7)
      @visitors[i].learns(@cro)
      @visitors[i].teaches(@eng)
      @visitors[i].learns(@ger)
      @visitors[i].learns(@spa)
    end
    for i in (8..9)
      @visitors[i].learns(@ara)
      @visitors[i].learns(@swe)
      @visitors[i].learns(@rus)
      @visitors[i].learns(@chi)
      @visitors[i].teaches(@fre) # feasible
      @visitors[i].teaches(@ita) # feasible
      @visitors[i].teaches(@ger) # feasible
      @visitors[i].teaches(@spa) # feasible
    end

  end#setup_event_visitors

  before :each do
    setup_event_visitors
  end

  # Tests that the manager creates a tribe schedule
  it "creates a schedule" do
    area_size = 6
    area_count = 3
    manager = Manager.new(area_size, area_count)
    for vis in @visitors
      manager.add_visitor(vis)
    end
    schedule = manager.make_schedule

    # puts Optimizer::schedule_inspect(schedule)
    # for kwh in @knowhows
    #   puts kwh.name + " " + kwh.id.to_s
    # end

    trib_knowhows = []
    for rd in schedule
      for ar in rd
        trib_knowhows << ar.knowhow_id
      end
    end

    expect(trib_knowhows).to include(@cro.id)
    expect(trib_knowhows).to include(@eng.id)
    expect(trib_knowhows).to include(@fre.id)
    expect(trib_knowhows).to include(@ita.id)
    expect(trib_knowhows).to include(@ger.id)
    expect(trib_knowhows).to include(@spa.id)

    expect(trib_knowhows).not_to include(@ara.id)
    expect(trib_knowhows).not_to include(@swe.id)
    expect(trib_knowhows).not_to include(@chi.id)
    expect(trib_knowhows).not_to include(@rus.id)
  end

end

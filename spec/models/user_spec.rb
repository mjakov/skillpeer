require "rails_helper"

RSpec.describe User, :type => :model do
  it "has field requirements for valid user" do
    lindeman = User.create!(email: "lindeman@example.com", name: "Andy Lindeman",
      password: "somepassword1");

    expect(lindeman).to be_valid
  end
end

require 'rails_helper'

RSpec.describe Skill, type: :model do

  before :each do
    @user1 = User.create(email: "huck@example.com", name: "Huck Finn",
      password: "somepassword1")
    @user2 = User.create(email: "lucky@example.com", name: "Lucky Luke",
      password: "somepassword1")
    @knowhow1 = Knowhow.create(name: "English")
    @knowhow2 = Knowhow.create(name: "French")
  end

  it "is invalid if user_id and knowhow_id are both duplicates" do
    expect {
      Skill.create(user_id: @user1.id, knowhow_id: @knowhow1.id)
    }.to change { Skill.count }.by(1)

    expect {
      Skill.create(user_id: @user1.id, knowhow_id: @knowhow2.id)
    }.to change { Skill.count }.by(1)

    expect {
      begin
        Skill.create(user_id: @user1.id, knowhow_id: @knowhow1.id)
      rescue => ex
        #puts "Testing exception: #{ex.class} #{ex.message}"
      end
    }.to_not change { Skill.count }
  end#it

  it "is invalid if user_id is nil" do
    expect {
      skill1 = Skill.create(user_id: nil, knowhow_id: @knowhow1.id)
      expect(skill1.valid?).to eq(false)
    }.to_not change { Skill.count }
  end

  it "is invalid if knowhow_id is nil" do
    expect {
      skill1 = Skill.create(user_id: @user1.id, knowhow_id: nil)
      expect(skill1.valid?).to eq(false)
    }.to_not change { Skill.count }
  end

  it "has a teaching field with a default value false" do
    skill1 = Skill.create(user_id: @user1.id, knowhow_id: @knowhow1.id)
    expect(skill1.teaching).to eq(false)
    skill1.teaching = true
    skill1.save
    expect(skill1.teaching).to eq(true)
  end

  it "can be created for a user" do
    expect {
      @user1.skills.create(knowhow_id: @knowhow1.id)
    }.to change { @user1.skills.count }.by(1)
  end

  it "is obtainable for some knowhow as a list" do
    expect {
      @user1.skills.create(knowhow_id: @knowhow1.id)
    }.to change { @knowhow1.skills.count }.by(1)
  end

  it "is invalid if there is already another skill with the same knowhow for that user" do
    expect {
      @user1.skills.create(knowhow_id: @knowhow1.id)
    }.to change { @user1.skills.count }.by(1)
    dup_skill = @user1.skills.build(knowhow_id: @knowhow1.id)
    expect(dup_skill.valid?).to eq(false)
  end

  it "is valid if there is already another skill with the same knowhow for a different user" do
    expect {
      @user1.skills.create(knowhow_id: @knowhow1.id)
    }.to change { @user1.skills.count }.by(1)
    dup_skill = @user2.skills.build(knowhow_id: @knowhow1.id)
    expect(dup_skill.valid?).to eq(true)
    expect {
      dup_skill.save
    }.to change { @user2.skills.count }.by(1)
  end

end

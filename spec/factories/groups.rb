require 'faker'

# Only the build(:group) method should be used, because the facilitator is missing
FactoryGirl.define do
  factory :group do
    name { Faker::Commerce.department(2, true) }
    location_id { 1 }
    max_places { Faker::Number.between(1, 50) }
    description { Faker::Company.catch_phrase }
    area_size { Faker::Number.between(2, 15) }
    area_count { Faker::Number.between(1, 10) }
  end
end

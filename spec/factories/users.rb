require 'faker'

FactoryGirl.define do
  factory :user do
    email {  Faker::Internet.email }
    name  { Faker::Name.name }
    password { "somepassword1" }
  end
end

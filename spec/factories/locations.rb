FactoryGirl.define do
  factory :location do
    name { Faker::University.name }
    country { Faker::Address.country }
    postnr { Faker::Address.postcode }
    city { Faker::Address.city }
    street { Faker::Address.street_name + Faker::Address.secondary_address }
    number { Faker::Number.between(1, 1000) }
  end
end

require 'faker'

FactoryGirl.define do

  factory :event do
    time_start { DateTime.tomorrow + 17.hours }
    time_end { DateTime.tomorrow + 20.hours }
    group_id :group
  end

end

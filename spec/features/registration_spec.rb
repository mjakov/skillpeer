require 'rails_helper'

feature "Registration" do

  scenario "New user registers with correct data" do
    visit root_path
    click_link "Sign up"
    fill_in "Name", with: "Marko Markovic"
    fill_in "Email", with: "markovic@example.com"
    fill_in "Password", with: "abcdefoaeudaocud8"
    click_button "Sign up"
    expect(page).to have_text("Marko")
  end

end

require 'rails_helper'

feature "Skill management" do

  before :all do
    @visitor1 = create(:user)
    @knowhow1 = Knowhow.create(name: "Chinese")
  end

  # Logged in user adds new skill
  scenario "new skill" do
    sign_in(@visitor1)
    click_link "Profile"
    click_link "Add skill"
    expect(current_path).to eq(new_user_skill_path(@visitor1))
    select @knowhow1.name, :from => "skill[knowhow_id]"
    click_button "Create Skill"
    expect(current_path).to eq(user_path(@visitor1))
    expect(page).to have_text(@knowhow1.name)
  end

  # Logged in user tries to add the same skill twice
  scenario "twice the same new skill" do
    sign_in(@visitor1)
    click_link "Profile"
    click_link "Add skill"
    expect(current_path).to eq(new_user_skill_path(@visitor1))
    #expect(page).to have_title(/Skill/)
    select @knowhow1.name, :from => "skill[knowhow_id]"
    click_button "Create Skill"
    expect(current_path).to eq(user_path(@visitor1))
    expect(page).to have_text(@knowhow1.name)
    click_link "Add skill"
    expect(current_path).to eq(new_user_skill_path(@visitor1))
    select @knowhow1.name, :from => "skill[knowhow_id]"
    click_button "Create Skill"
    expect(page).to have_text("Skill not created")
    expect(page).to have_text("should be unique")
  end

  scenario "delete skill" do
    sign_in(@visitor1)
    expect(@visitor1.skills.count).to eq(0)
    click_link "Profile"
    click_link "Add skill"
    select @knowhow1.name, :from => "skill[knowhow_id]"
    click_button "Create Skill"
    expect(@visitor1.skills.count).to eq(1)
    expect(current_path).to eq(user_path(@visitor1))
    expect(page).to have_button("Delete")
    click_button("Delete")
    expect(@visitor1.skills.count).to eq(0)
    expect(current_path).to eq(user_path(@visitor1))
    expect(page).not_to have_text(@knowhow1.name)
    expect(page).to have_text("Skill deleted")
  end

end

require 'rails_helper'

feature "Login and logout" do

  before :all do
    @vis1 = create(:user)
    @fac1 = build(:user)
    @fac1.make_facilitator
    @fac1.save
    @grp1 = build(:group)
    @grp1.facilitator = @fac1
    @grp1.save
    @grp2 = build(:group)
    @grp2.facilitator = @fac1
    @grp2.save
  end

  after :all do
    @vis1.destroy
    @grp1.destroy
    @grp2.destroy
    @fac1.destroy
  end

  scenario "Visitor logs in and immediately logs out" do
    current_user=nil
    visit root_path
    click_link "Login"
    fill_in "email", with: @vis1.email
    fill_in "password", with: @vis1.password
    click_button "Log in"
    expect(page).to have_text(/Welcome/)
    click_button "Logout"
    expect(page).to have_text(/See you/)
  end

  scenario "Visitor logs in, browses the site and logs out" do
    current_user=nil
    visit root_path
    click_link "Login"
    fill_in "email", with: @vis1.email
    fill_in "password", with: @vis1.password
    click_button "Log in"
    expect(page).to have_text(/Welcome/)
    click_link "Groups"
    expect(page).to have_text(/Groups/)
    click_link @grp1.name
    expect(page).to have_text(@grp1.location.street)
    click_link "Profile"
    click_link "Groups"
    click_button "Logout"
    expect(page).to have_text(/See you/)
  end

end

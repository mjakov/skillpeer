require 'rails_helper'

feature "Create group" do

  before :all do
    @fac1 = create(:user)
    @fac1.role = 4;
    @fac1.save
  end

  after :each do
    @fac1.destroy
  end

  scenario "Logged in facilitator creates new group" do
    sign_in(@fac1)
    click_link "Groups"
    click_link "New Group"
    expect {
      fill_in 'group_name', with: "New Language Group"
      fill_in 'group_max_places',with: "30"
      fill_in 'group_description', with: "A"*100
      fill_in 'group_area_size', with: "6"
      fill_in 'group_area_count', with: "5"
      fill_in 'group_location_name', with: "Pivnica Pinta"
      fill_in 'group_location_street', with: "Radiceva"
      fill_in 'group_location_number', with: "1a"
      fill_in 'group_location_city', with: "Zagreb"
      fill_in 'group_location_postnr', with: "10000"
      fill_in 'group_location_country', with: "Croatia"
      click_button "Create Group"
    }.to change { Group.count }.by(1)
    expect(current_path).to eql(root_path)
    expect(page).to have_text(/group created/)
    sign_out()
  end#scenario

  scenario "Logged in facilitator creates new group with new location" do
    sign_in(@fac1)
    visit groups_path
    click_link "New Group"
    oldLocCount = Location.count
    expect {
      fill_in 'group_name', with: "New Language Group"
      fill_in 'group_max_places',with: "30"
      fill_in 'group_description', with: "A"*100
      fill_in 'group_area_size', with: "6"
      fill_in 'group_area_count', with: "5"
      fill_in 'group_location_name', with: "Nova Pivnica Pinta"
      fill_in 'group_location_street', with: "Radiceva"
      fill_in 'group_location_number', with: "1a"
      fill_in 'group_location_city', with: "Zagreb"
      fill_in 'group_location_postnr', with: "10000"
      fill_in 'group_location_country', with: "Croatia"
      click_button "Create Group"
    }.to change { Group.count }.by(1)
    expect(Location.count).to eq(oldLocCount + 1)
    expect(current_path).to eql(root_path)
    expect(page).to have_text(/group created/)
    click_link "Groups"
    expect(page).to have_text("New Language Group")
    sign_out()
  end #scenario

  scenario "Logged in facilitator tries to create a new group with incorrect area data" do
    sign_in(@fac1)
    click_link "Groups"
    click_link "New Group"
    expect {
      fill_in 'group_name', with: "New Language Group"
      fill_in 'group_max_places',with: "30"
      fill_in 'group_description', with: "A"*100
      fill_in 'group_area_size', with: "1"
      fill_in 'group_area_count', with: "7"
      click_button "Create Group"
    }.not_to change { Group.count }
    expect(page).to have_text(/group not created/)
    sign_out
  end#scenario
end#feature

feature "Update group" do

  before :each do
    @fac1 = create(:user)
    @fac1.role = 4;
    @fac1.save
    @group1 = build(:group)
    @group1.facilitator = @fac1
    @group1.area_size = 9;
    @group1.save
  end

  after :each do
    @group1.destroy
    @fac1.destroy
  end

  scenario "Logged in facilitator updates group with correct data" do
    sign_in(@fac1)
    click_link "Groups"
    click_link @fac1.groups.first.name
    click_link "Edit"
    expect(current_path).to eql(edit_group_path(@group1))
    oldAreaSize = @group1.area_size
    fill_in 'group_area_size', with: "12"
    click_button "Update Group"
    @group1.reload
    expect(@group1.area_size).not_to eql(oldAreaSize)
    expect(@group1.area_size).to eql(12)
    expect(page).to have_content(/Updated/)
    sign_out
  end#scenario

  scenario "Logged in facilitator tries to update group with incorrect data" do
    sign_in(@fac1)
    click_link "Groups"
    click_link @fac1.groups.first.name
    click_link "Edit"
    expect(current_path).to eql(edit_group_path(@group1))
    oldAreaSize = @group1.area_size
    fill_in 'group_area_size', with: "-5"
    click_button "Update Group"
    @group1.reload
    expect(@group1.area_size).to eql(oldAreaSize)
    expect(page).to have_content(/Not Updated/)
    expect(page).to have_xpath('.//div[@class="error_messages"]/ul/li[@class="error_item"]')
    sign_out
  end#scenario
end#feature

feature "Open group" do

  before :each do
    @fac1 = create(:user)
    @fac1.role = 4;
    @fac1.save
    @group1 = build(:group)
    @group1.facilitator = @fac1
    @group1.area_size = 9;
    @group1.save
    @usr1 = create(:user)
  end

  after :each do
    @group1.destroy
    @usr1.destroy
    @fac1.destroy
  end

  scenario "User opens group" do
    sign_in(@usr1)
    click_link "Groups"
    click_link @group1.name
    expect(page).to have_text(@group1.name)
    expect(page).to have_text(@group1.location.name)
    expect(page).to have_text(@group1.location.street)
    expect(page).to have_text(@group1.max_places)
    expect(page).not_to have_text("Area Size")
    expect(page).not_to have_text("Area Count")
    sign_out
  end#scenario

  scenario "Facilitator opens group" do
    sign_in(@fac1)
    click_link "Groups"
    click_link @group1.name
    expect(page).to have_text(@group1.name)
    expect(page).to have_text(@group1.location.name)
    expect(page).to have_text(@group1.location.street)
    expect(page).to have_text(@group1.max_places)
    expect(page).to have_text(/Area Size.*#{@group1.area_size}/)
    expect(page).to have_text(/Area Count.*#{@group1.area_count}/)
    sign_out
  end#scenario

end#feature

feature "Delete Group" do

  before :each do
    @fac1 = create(:user)
    @fac1.role = 4;
    @fac1.save
    @group1 = build(:group)
    @group1.facilitator = @fac1
    @group1.area_size = 10;
    @group1.save
    tstart = DateTime.now+2.days
    tend = tstart + 3.hours
    evt = @group1.events.build(time_start: tstart, time_end: tend)
    evt.save
  end

  after :each do
    @group1.destroy
    @fac1.destroy
  end

  scenario "Facilitator deletes group" do

    sign_in(@fac1)
    click_link "Groups"
    click_link @group1.name
    expect{
      click_button "Delete"
    }.to change { Group.count }.by(-1)
    expect(page).to have_text(/deleted/)
    sign_out()
  end #scenario

  scenario "Facilitator deletes group with its events" do

    sign_in(@fac1)
    click_link "Groups"
    click_link @group1.name
    expect{
      click_button "Delete"
    }.to change { Event.count }.by(-1)
    sign_out()
  end #scenario

end #feature

feature "Update group location" do

  before :each do
    @fac1 = create(:user)
    @fac1.role = 4;
    @fac1.save
    @group1 = build(:group)
    @group1.facilitator = @fac1
    @group1.save
    @usr1 = create(:user)
    @locBad = Location.new(name:"A", country: "Cr", postnr:"1", city:"Z",
      street: "U", number: "1")
    @locGood = Location.new(name:"Another good location", country: "Croatia",
        postnr:"10000", city:"Zagreb",
        street: "Mlinarska", number: "105")
  end

  after :each do
    @group1.destroy
    @usr1.destroy
    @fac1.destroy
    @locBad.destroy
    @locGood.destroy
  end

  scenario "Update group with bad new location" do
    sign_in(@fac1)
    oldLoc = @group1.location
    click_link "Groups"
    click_link @group1.name
    click_link "Edit", href: /group/#{Regexp.quote(@group1.id.to_s)}/
    expect {
      fill_in "group_location_name", with: @locBad.name
      fill_in "group_location_street", with: @locBad.street
      fill_in "group_location_number", with: @locBad.number
      fill_in "group_location_city", with: @locBad.city
      fill_in "group_location_postnr", with: @locBad.postnr
      fill_in "group_location_country", with: @locBad.country
      click_button "Update Group"
    }.not_to change{Location.count}
    @group1.reload
    expect(@group1.location).to eq(oldLoc)
    expect(page).to have_text(/Edit Group/)
    sign_out()
  end

  scenario "Update group with valid new location" do
    sign_in(@fac1)
    oldLoc = @group1.location
    click_link "Groups"
    click_link @group1.name
    click_link "Edit", href: /group/#{Regexp.quote(@group1.id.to_s)}/
    expect {
      fill_in "group_location_name", with: @locGood.name
      fill_in "group_location_street", with: @locGood.street
      fill_in "group_location_number", with: @locGood.number
      fill_in "group_location_city", with: @locGood.city
      fill_in "group_location_postnr", with: @locGood.postnr
      fill_in "group_location_country", with: @locGood.country
      click_button "Update Group"
    }.to change { Location.count }.by(1)
    @group1.reload
    expect(@group1.location).not_to eq(oldLoc)
    sign_out()
  end

  scenario "Update group with other existing location" do
    @locGood.save
    sign_in(@fac1)
    oldLoc = @group1.location
    click_link "Groups"
    click_link @group1.name
    click_link "Edit", href: /group/#{Regexp.quote(@group1.id.to_s)}/
    expect {
      fill_in "group_location_name", with: @locGood.name
      fill_in "group_location_street", with: @locGood.street
      fill_in "group_location_number", with: @locGood.number
      fill_in "group_location_city", with: @locGood.city
      fill_in "group_location_postnr", with: @locGood.postnr
      fill_in "group_location_country", with: @locGood.country
      click_button "Update Group"
    }.not_to change { Location.count }
    @group1.reload
    expect(@group1.location).not_to eq(oldLoc)
    expect(@group1.location).to eq(@locGood)
    sign_out()
  end

end # feature

feature "Groups index" do

  before :each do
    @locations = []
    @groups = []
    @fac1 = build(:user)
    @fac1.make_facilitator
    @fac1.save
    @fac2 = build(:user)
    @fac2.make_facilitator
    @fac2.save

    for i in 0..4
      @locations[i] = build(:location)
    end
    @locations[0].country = "Brazil"
    @locations[1].country = "Argentina"
    @locations[2].country = "Brazil"
    @locations[3].country = "Chile"
    @locations[4].country = "Argentina"
    for i in 0..4
      @locations[i].save
    end

    for i in 0..3
      @groups[i] = build(:group)
      @groups[i].facilitator = @fac1
    end
    for i in 4..9
      @groups[i] = build(:group)
      @groups[i].facilitator = @fac2
    end
    for i in 0..3
      @groups[2*i].location = @locations[i]
      @groups[2*i + 1].location = @locations[i]
    end
    for grp in @groups
      grp.save
    end
  end #before

  after :each do
    for loc in @locations
      loc.destroy
    end
    for grp in @groups
      grp.destroy
    end
    @fac1.destroy
    @fac2.destroy
  end #after

  scenario "Non-logged in user goes to group index page" do
    visit groups_path
    expect(current_path).to eql(groups_path)
    @groups.each do |grp|
      expect(page).to have_text(grp.name)
      expect(page).to have_text(grp.location.country)
      expect(page).to have_text(grp.location.city)
    end
  end # scenario

  scenario "Facilitator only sees his own groups on the index page" do
    sign_in(@fac1)
    visit groups_path
    expect(current_path).to eql(groups_path)
    @groups.take(4).each do |grp|
      expect(page).to have_text(grp.name)
      expect(page).to have_text(grp.location.country)
      expect(page).to have_text(grp.location.city)
    end
    @groups[4..9].each do |grp|
      expect(page).not_to have_text(grp.name)
    end
    expect(page).to have_text(@locations[0].country)
    expect(page).to have_text(@locations[1].country)
    expect(page).to have_text(@locations[2].country)
    expect(page).not_to have_text(@locations[3].country)
    sign_out
  end # scenario

end #feature

require 'rails_helper'

DTIME_FORMAT = "%Y/%m/%d %H:%M"

def given_a_facilitator
  facilitator = build(:user)
  facilitator.role = 4
  facilitator.save
  return facilitator
end

def given_a_group(facilitator)
  group = build(:group)
  group.facilitator_id = facilitator.id
  group.save
  return group
end

def given_an_event(group)
  event = build(:event)
  event.group_id = group.id
  event.save
  group.events << event
  group.save
  return event
end

def visit_groups_page
  visit root_path
  click_link "Groups"
end

def visit_group_page(group)
  visit_groups_page
  click_link group.name
  expect(current_path).to eq(group_path(group))
end


feature "Event management" do

  scenario "create event" do

    facilitator = given_a_facilitator
    group = given_a_group(facilitator)

    visit root_path

    click_link 'Login'
    expect(current_path).to eq(new_login_path)

    fill_in 'email', with: facilitator.email
    fill_in 'password', with: facilitator.password
    click_button "Log in"

    visit root_path

    click_link "Groups"

    expect(current_path).to eq(groups_path)
    click_link group.name
    expect(current_path).to eq(group_path(group))

    expect {
      click_link "New Event"
      expect(current_path).to eq(new_group_event_path(group))
      tmSt = DateTime.tomorrow + 11.hours
      tmEd = DateTime.tomorrow + 14.hours

      fill_in "dtime_start", with: tmSt.strftime("%Y/%m/%d %H:%M")
      fill_in "dtime_end", with: tmEd.strftime("%Y/%m/%d %H:%M")

      click_button "Create Event"
    }.to change { Event.count }.by(1)
  end#scenario create event

  scenario "Logged in visitor attends event" do
    facilitator = given_a_facilitator
    group = given_a_group(facilitator)
    group.max_places = 1;
    group.save
    event1 = given_an_event(group)
    visitor = create(:user)
    othVisitor = create(:user)
    sign_in(visitor)

    visit root_path
    click_link "Groups"
    click_link group.name
    expect(page).to have_css("td.free_places", text: "#{group.max_places}")
    click_button "Show", :match => :first
    expect {
      click_button "Interested"
    }.to change { event1.users.count }.by(1)
    click_link "Groups"
    click_link group.name
    expect(page).to_not have_css("td.free_places", text: "#{group.max_places}")
    expect(page).to have_css("td.free_places", text: "#{group.max_places - 1}")
    sign_out

    sign_in(othVisitor)
    visit_group_page(group)
    click_button "Show", :match => :first
    expect(page).to have_css("input[type='submit'][name='commit'][disabled]")
  end

  scenario "Non-logged-in user sees attend button but it redirects to login page" do
    facilitator = given_a_facilitator
    group = given_a_group(facilitator)
    visitor = create(:user)
    event = given_an_event(group)
    visit group_event_path(group, event)
    click_link "Interested"
    expect(current_path).to eq(new_login_path)
    sign_in(visitor)
    visit group_event_path(group, event)
    click_button "Interested"
    expect(page).to have_text("Your name was added to the guest list.")
  end

  scenario "Visitor sees no unattend button unless he is attending" do
    facilitator = given_a_facilitator
    group = given_a_group(facilitator)
    visitor = create(:user)
    event = given_an_event(group)
    sign_in(visitor)
    visit group_event_path(group, event)
    expect(page).to have_css("input[type='submit'][value='Interested']")
    expect(page).to have_css("input[type='submit'][value='Not interested'][disabled='disabled']")
    click_button "Interested"
    expect(page).to have_css("input[type='submit'][value='Not interested']")
    expect(page).to have_css("input[type='submit'][value='Interested'][disabled='disabled']")
  end

  # Facilitator creates event

end#feature EventManagement

feature "Event management 2" do

  before :each do
    @facilitator1 = create(:user)
    @facilitator1.role = 4
    @facilitator1.save

    @group1 = build(:group)
    @group1.facilitator_id = @facilitator1.id
    @group1.save

    @events = []
    for i in 1..3
      event = build(:event)
      event.group_id = @group1.id
      event.time_start = DateTime.tomorrow + (2 * i).hours
      event.time_end = DateTime.tomorrow + (2 * i + 1).hours
      event.save
      @events << event
    end
    expect(@events.length).to eq(3)

    @visitor1 = create(:user)

    @facilitator2 = create(:user)
    @facilitator2.role = 4
    @facilitator2.save
  end

  scenario "Facilitator lists events to see if they are all displayed" do
    sign_in(@facilitator1)
    visit_group_page(@group1)
    @events.each do |evt|
      expect(page).to have_css("td", text: evt.time_start.strftime("%d %B %Y"))
      expect(page).to have_css("td", text: evt.time_start.strftime("%H:%M"))
    end
    sign_out
  end#scenario

  scenario "All events for the group are displayed to the non-registered user" do
    visit_group_page(@group1)
    @events.each do |evt|
      within 'table' do
        expect(page).to have_css("td", text: evt.time_start.strftime("%d %B %Y"))
        expect(page).to have_css("td", text: evt.time_start.strftime("%H:%M"))
        expect(page).to have_css("td", text: evt.time_end.strftime("%H:%M"))
      end
    end
  end#scenario

  scenario "Wrong data for event edit gives an error message" do
    sign_in(@facilitator1)
    visit edit_group_event_path(@group1, @events[0])
    tmStOld = @events[0].time_end
    fill_in "dtime_start", with: tmStOld.change(min: 22).strftime(DTIME_FORMAT)
    click_button "Update Event"
    expect(page).to have_content "Event update failed"
    expect(page).to have_content /Time end.*time start/
  end

  scenario "Correct updated data is displayed in events list" do
    sign_in(@facilitator1)
    visit edit_group_event_path(@group1, @events[0])
    oldTmStart = @events[0].time_start
    fill_in :dtime_start, with: (oldTmStart + 30.minutes).strftime("%Y/%m/%d %H:%M")
    click_button "Update Event"
    expect(page).to have_content /success/
    @events[0].reload
    newTmStart = @events[0].time_start
    expect(newTmStart).to eq (oldTmStart + 30.minutes)
    expect(page).to have_content newTmStart.strftime("%H:%M")
  end

  scenario "Deleted event is no longer displayed in the event list" do
    sign_in(@facilitator1)
    visit_group_page(@group1)
    within(".events") do
      within("##{@events[0].id}") do
        click_button "Show"
      end
    end
    click_button "Delete"
    visit_group_page(@group1)
    expect(page).to_not have_css ".events tr##{@events[0].id}"
  end
end#feature Event management 2

feature "Event statistics page" do

  before :each do
    setup_user_skill_events_2
  end

  after :each do
    destroy_user_skill_events_2
  end

  scenario "should display all relevant data" do
    sign_in(@thomas_jefferson)
    click_link "Groups"
    click_link @meetingGp1.name
    event_show_button_id = "event#{@meetingGp1Ev1.id}"
    click_button event_show_button_id
    expect(current_path).to eq(group_event_path(@meetingGp1, @meetingGp1Ev1))
    expect(page).to have_text(/Skills/)
    expect(page).to have_text("Croatian")
    expect(page).to have_text("English")
    expect(page).to have_text("German")
  end

  scenario "should display all relevant data for groups with the same location" do
    @meetingGp2.location = @location1
    @meetingGp2.save
    sign_in(@thomas_jefferson)
    click_link "Groups"
    click_link @meetingGp1.name
    event_show_button_id = "event#{@meetingGp1Ev1.id}"
    click_button event_show_button_id
    expect(current_path).to eq(group_event_path(@meetingGp1, @meetingGp1Ev1))
    expect(page).to have_text(/Skills/)
    expect(page).to have_text("Croatian")
    expect(page).to have_text("English")
    expect(page).to have_text("German")
  end


end #feature Event statistics

feature "Event guest list" do

  scenario "Owning facilitator sees the guest list" do
    setup_user_skill_events_2
    sign_in(@tom_sawyer)
    click_link("Groups")
    click_link(@meetingGp1.name)
    click_button("event#{@meetingGp1Ev1.id}")
    expect(current_path).to eq(group_event_path(@meetingGp1, @meetingGp1Ev1))
    click_link("Guest List")
    expect(current_path).to eq(event_visitors_path(@meetingGp1, @meetingGp1Ev1))
    expect(page).to have_text("Guest List")
    @meetingGp1Ev1.users.each do |visitor|
      expect(page).to have_text(visitor.name)
    end
    sign_out()
    destroy_user_skill_events_2
  end

  scenario "Now-owning facilitator can't access event page" do
    setup_user_skill_events_2
    sign_in(@mike_tyson)
    visit(group_event_path(@meetingGp1, @meetingGp1Ev1))
    expect(current_path).to eq(root_path)
    sign_out()
    destroy_user_skill_events_2
  end

  scenario "Visitor does not see the guests" do
    setup_user_skill_events_2
    sign_in(@thomas_jefferson)
    click_link("Groups")
    click_link(@meetingGp1.name)
    click_button("event#{@meetingGp1Ev1.id}")
    expect(current_path).to eq(group_event_path(@meetingGp1, @meetingGp1Ev1))
    expect(page).not_to have_text("Guest List")
    sign_out()
    destroy_user_skill_events_2
  end#scenario
end#feature Event guest list

feature "Event agenda" do

  before :each do
    setup_user_skill_events_2
  end

  after :each do
    destroy_user_skill_events_2
  end

  context "Not logged-in user" do
    scenario "does not see the agenda button" do
      visit root_path
      click_link("Groups")
      click_link(@meetingGp1.name)
      click_button("Show", match: :first)
      expect(page).not_to have_button("Create Agenda")
    end
  end

  context "Logged-in user" do
    scenario "does not see the agenda button" do
      sign_in(@mark_twain)
      click_link("Groups")
      click_link(@meetingGp1.name)
      click_button("Show", match: :first)
      expect(page).not_to have_button("Create Agenda")
      sign_out
    end
  end

  context "Facilitator" do
    scenario "sees the agenda button and the generated agenda" do
      sign_in(@tom_sawyer)
      click_link("Groups")
      click_link(@meetingGp1.name)
      click_button("Show", match: :first)
      expect(page).to have_button("Create Agenda")
      click_button("Create Agenda")
      expect(page).to have_css("h1", :text => /Agenda/)

      expect(page).to have_text(/Croatian/)
      expect(page).to have_text(/English/)

      sign_out
    end #scenario

    scenario "facilitator creates agenda for event with too few visitors" do
      sign_in(@mike_tyson)
      visit group_event_path(@meetingGp2, @meetingGp2Ev3)
      click_button("Create Agenda")
      expect(current_path).to eql(group_event_path(@meetingGp2, @meetingGp2Ev3))
      expect(page).to have_text(/no agenda possible/)
      sign_out
    end

  end #context

end #feature Event agenda

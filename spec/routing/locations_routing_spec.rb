require "rails_helper"

describe LocationsController do

  describe "routing" do

    it "routes to #index" do
      expect(get: "/locations").to route_to(
        :controller => "locations",
        :action => "index")
    end

  end#describe "routing"
end#describe LocationsController

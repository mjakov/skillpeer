require "rails_helper"

describe EventsController do

  describe "routing" do

    it "routes to #agenda" do
      expect(post: "/groups/1/events/2/agenda").to route_to(
        :controller => "events",
        :action => "agenda",
        :group_id => "1",
        :id => "2")
    end

    # Guestlist
    it "route to #guests" do
      expect(get: "/groups/1/events/2/visitors").to route_to(
      :controller => "events",
      :action => "visitors",
      :group_id => "1",
      :id => "2")
    end

  end#describe "routing"
end#describe EventsController

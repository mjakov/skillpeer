ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  @@visitor_id = 1

  # Add more helper methods to be used by all tests here...

  def login_as(user)
    post login_path, params: { email: user.email, password: user.password}
  end

  def logout
    delete login_path
  end

  def create_visitor
    visitor = User.create!({name: "Visitor#{@@visitor_id}", email: "visitor#{@@visitor_id.to_s}@example.com", password: "password1"})
    @@visitor_id += 1;
    return visitor
  end

end

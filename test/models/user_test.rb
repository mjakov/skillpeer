require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @nmStr = "Thomas Mann"
    @emStr = "thomas@example.com"
    @pwStr = "secret3"
  end

  def test_user
    nmStr = "Thomas Mann"
    emStr = "thomas@example.com"
    pwStr = "secret"
    user = User.new({name: nmStr, email: emStr, password: pwStr})
    assert_equal(nmStr, user.name)
    assert_equal(emStr, user.email)
    assert_equal(pwStr, user.password)
  end

  def test_name_presence
    nmStr = "Thomas Mann"
    emStr = "thomas@example.com"
    pwStr = "secret3"
    user = User.new({name: nil, email: emStr, password: pwStr})
    assert_not(user.valid?, "User with an empty name should be invalid")
  end

  def test_name_length
    nmStr = "Tom"
    emStr = "thomas@example.com"
    pwStr = "secret3"
    user = User.new({name: nmStr, email: emStr, password: pwStr})
    assert_not(user.valid?, "Name should be longer than 3 characters")
    nmStr = "Thomas Ricardo Gonzales Garcia The Third"
    user = User.new({name: nmStr, email: emStr, password: pwStr})
    assert_not(user.valid?, "Name should be shorter than 31 characters")
    nmStr = "Thomas Ricardo Gonzales Garcia"
    user = User.new({name: nmStr, email: emStr, password: pwStr})
    assert(user.valid?, "A name between 4 and 30 characters should be valid")
  end

  def test_password_validity
    nmStr = "Thomas Mann"
    emStr = "thomas@example.com"
    pwStr = nil
    user = User.new({name: nmStr, email: emStr, password: pwStr})
    assert_not(user.valid?, "Password should not be empty")
    pwStr = "abcd"
    user = User.new({name: nmStr, email: emStr, password: pwStr})
    assert_not(user.valid?, "Password should be at least 5 characters")
    pwStr = "1234567890123456789012345678901"
    user = User.new({name: nmStr, email: emStr, password: pwStr})
    assert_not(user.valid?, "Password should be less than 31 characters")
    pwStr = "password should be ok"
    user = User.new({name: nmStr, email: emStr, password: pwStr})
    assert(user.valid?, "Password should be ok")
  end

  def test_email_validity
    user = User.new({name: @nmStr, email: nil, password: @pwStr})
    assert_not(user.valid?, "Empty email should not be valid")

    @emStr = "thomas at example.com"
    user = User.new({name: @nmStr, email: @emStr, password: @pwStr})
    assert_not(user.valid?, "Email without monkey should not be valid")

    @emStr = "thomas@example dot com"
    user = User.new({name: @nmStr, email: @emStr, password: @pwStr})
    assert_not(user.valid?, "Email without dot should not be valid")

    @emStr = "thomas@example.ac.at"
    user = User.new({name: @nmStr, email: @emStr, password: @pwStr})
    assert(user.valid?, "Complex email should be valid")

    @emStr = "tho-mas@example.ac.at"
    user = User.new({name: @nmStr, email: @emStr, password: @pwStr})
    assert(user.valid?, "Email with non word characters should be valid")

    @emStr = "1234567890123456789012345678901234567890@example.ac.at"
    user = User.new({name: @nmStr, email: @emStr, password: @pwStr})
    assert_not(user.valid?, "Email with more than 50 characters should not be valid")

    @emStr = "thomas@example.com"
    user = User.new({name: @nmStr, email: @emStr, password: @pwStr})
    assert(user.valid?, "Normal email should be valid")
  end

  def test_email_unique
    user1 = User.new({name: @nmStr, email: @emStr, password: @pwStr})
    assert(user1.valid?, "User should be valid")
    assert(user1.save, "User should be saved correctly")
    @nmStr = "Marc Faber"
    user2 = User.new({name: @nmStr, email: @emStr, password: @pwStr})
    assert_not(user2.valid?, "User should be invalid because email is not unique")
    assert_not(user2.save, "User should not be saved correctly")
    user1.destroy
    assert(user2.valid?, "User should be valid because email is unique")
    assert(user2.save, "User should be saved correctly")
    user2.destroy
  end

  test "test user without email should not work" do
    user1 = User.new({name: @nmStr, email: nil, password: @pwStr})
    assert_not(user1.valid?, "User should not be valid")
  end

  def test_authenticate_nonexisting_user
    emlSt = "nonexisting@example.com"
    pwdSt = "nonexistingpassword1"
    user = User.authenticate(emlSt, pwdSt)
    assert_equal(nil, user, "User should be nil because it does not exist")
  end

  def test_authenticate_user_with_wrong_password
    userNew = User.new({name: @nmStr, email: @emStr, password: @pwStr})
    assert(userNew.save)
    userGot = User.authenticate(@emStr, @pwStr + "hey")
    assert_equal(nil, userGot, "User should be nil because password is wrong")
  end

  def test_authenticate_user_with_correct_password
    userNew = User.new({name: @nmStr, email: @emStr, password: @pwStr})
    assert(userNew.save)
    userGot = User.authenticate(@emStr, @pwStr)
    assert_equal(userNew, userGot, "Userl should authenticate correctly")
  end

  def test_roles
    user = User.new({name: @nmStr, email: @emStr, password: @pwStr,
      role: User.roles([:BANNED])})
    assert(user.banned?)
    user = User.new({name: @nmStr, email: @emStr, password: @pwStr,
      role: User.roles([:ADMIN])})
    assert(user.admin?)
    assert_equal("Admin", user.role_str, "User's role string should be Admin")
    user = User.new({name: @nmStr, email: @emStr, password: @pwStr,
      role: User.roles([:FACILITATOR])})
    assert(user.facilitator?)
    assert(user.role_str == "Facilitator")
    user = User.new({name: @nmStr, email: @emStr, password: @pwStr,
      role: User.roles([:VISITOR])})
    assert(user.visitor?)
    assert(user.role_str == "Visitor")
    user = User.new({name: @nmStr, email: @emStr, password: @pwStr,
      role: User.roles([:VISITOR, :BANNED])})
    assert(user.visitor?)
    assert(user.banned?)
    assert(user.role_str == "Visitor", "User's role str should be Visitor")
    assert_not(user.facilitator?)
    assert_not(user.admin?)
    user.make_facilitator
    assert(user.facilitator?, "User should be facilitator")
    user.unmake_facilitator
    assert_not(user.facilitator?)
  end

  test "facilitator should have some corresponding meeting groups" do
    facilitator = users(:tomsawyer)
    lacotabo = groups(:lacotabo)
    babilon = groups(:babilon)

    assert_equal(facilitator, lacotabo.facilitator)
    assert_equal(facilitator, babilon.facilitator)

    assert_equal(2, facilitator.groups.count)
    assert(facilitator.groups.include?(lacotabo))
    assert(facilitator.groups.include?(babilon))
  end

end

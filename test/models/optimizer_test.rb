require 'test_helper'
require 'agenda/tribe'
require 'agenda/optimizer'

include Agenda

class TestOptimizer < ActiveSupport::TestCase

  def setup
    @rds= 2
    @ars = 3
    @gps = []
    for i in (1..@ars)
      for j in (1..@rds)
        @gps << Tribe.new(i)
      end
    end
    @opt = Optimizer.new(@rds, @ars, @gps)
  end

  def test_init
    assert_equal(@rds, @opt.round_cnt)
    assert_equal(@ars, @opt.area_cnt)
    assert_equal(@rds, @opt.schedule.length)
    assert_equal(@ars, @opt.schedule[0].length)
    assert_equal(@ars * @rds, @opt.tribes.length)

    assert_equal(@opt.tribes[0].knowhow_id, @opt.tribes[1].knowhow_id)
    assert_not_equal(@opt.tribes[1].knowhow_id, @opt.tribes[2].knowhow_id)
  end

  # No tribes in the round have the same knowhow_id if this is possible
  def test_make_schedule_1
    cnt = 0
    for row in @opt.schedule
      for el in row
        assert_equal(nil, el)
        cnt += 1
      end
    end
    assert_equal(@rds * @ars, cnt)
    assert(@opt.tribes.length <= @opt.round_cnt * @opt.area_cnt)

    schedule = @opt.make_schedule

    # check that all slots have been filled
    for row in schedule
      for el in row
        assert_not_equal(nil, el)
      end
    end

    # check that there are no knowhow duplicates in a single round
    for rd in schedule
      for gp in rd
        knowId = gp.knowhow_id
        cnt = rd.count {|g| g.knowhow_id == knowId}
        assert_equal(1, cnt)
      end
    end

  end #test_make_schedule_1

  def test_make_schedule_2

    rds= 2
    ars = 3
    gps = []
    for i in (1..ars-1)
      for j in (1..rds)
        gps << Tribe.new(i)
      end
    end
    opt = Optimizer.new(rds, ars, gps)

    schedule = opt.make_schedule

    # check that there are no knowhow duplicates in a single round
    for rd in schedule
      for gp in rd
        if gp != nil
          knowId = gp.knowhow_id
          cnt = rd.count {|g| g != nil && g.knowhow_id == knowId}
          assert_equal(1, cnt)
        end
      end
    end

  end #test_make_schedule_2

end #TestOptimizer

require 'test_helper'

include Agenda

class TribeTest < ActiveSupport::TestCase

  def setup
    knowhow_id = 1
    @tribe = Tribe.new(knowhow_id)
    assert_equal(@tribe.knowhow_id, knowhow_id)
  end

  def test_init
    assert_equal(@tribe.learner_count, 0)
    assert_equal(@tribe.teacher_count, 0)
    assert_equal(@tribe.learner_list, [])
    assert_equal(@tribe.teacher_list, [])
  end

  def test_id_not_writable
    assert_raise NoMethodError do
      @tribe.knowhow_id = 8
    end
  end

  def test_id_readable
    assert_nothing_raised do
      knowhow_id = @tribe.knowhow_id
    end
  end

  def test_add_learner
    old_lnr_cnt = @tribe.learner_count
    lnr = User.new({name: "Michael", email: "michael@example.com", password: "password1"})
    @tribe.add_learner(lnr)
    assert_equal(@tribe.learner_count, old_lnr_cnt + 1)
    lners = @tribe.learner_list
    assert(lners.include?(lnr))
  end

  def test_add_teacher
    old_tchr_cnt = @tribe.teacher_count
    tchr = User.new({name: "Thomas", email: "thomas@example.com", password: "password1"})
    @tribe.add_teacher(tchr)
    tchrs = @tribe.teacher_list
    assert(tchrs.include?(tchr))
    assert_equal(@tribe.teacher_count, old_tchr_cnt + 1)
  end

  def test_feasible_1
    g1 = Tribe.new(1)
    assert_equal(false, g1.feasible?())
    for i in (1..5)
      visitor = User.new({name: "Visitor#{i}", email: "visitor#{i}@example.com", password: "password1"})
      g1.add_learner(visitor)
    end
    assert_equal(false, g1.feasible?())
    visitor = User.new({name: "Teacher", email: "teacher@example.com", password: "password1"})
    g1.add_teacher(visitor)
    assert_equal(true, g1.feasible?())
  end

  def test_feasible_2
    g1 = Tribe.new(1)
    assert_equal(false, g1.feasible?())
    for i in (1..5)
      teacher = User.new({name: "Teacher#{i}", email: "teacher#{i}@example.com", password: "password1"})
      g1.add_teacher(teacher)
    end
    assert_equal(false, g1.feasible?())
    learner = User.new({name: "Learner", email: "learner@example.com", password: "password1"})
    g1.add_learner(learner)
    assert_equal(true, g1.feasible?())
  end

  def test_size_estimate
    g1 = Tribe.new(3)
    assert_equal(0, g1.size_estimate)
    for i in (4..5)
      teacher = User.new({name: "Teacher#{i}", email: "teacher#{i}@example.com", password: "password1"})
      g1.add_teacher(teacher)
    end
    assert_equal(0, g1.size_estimate)
    for i in (1..3)
      visitor = User.new({name: "Visitor#{i}", email: "visitor#{i}@example.com", password: "password1"})
      g1.add_learner(visitor)
    end
    assert_equal(5, g1.size_estimate)
  end

  def test_too_big?
    area_size = 5
    g1 = Tribe.new(3)
    assert_equal(false, g1.too_big?(area_size))
    for i in (1..3)
      visitor = User.new({name: "Visitor#{i}", email: "visitor#{i}@example.com", password: "password1"})
      g1.add_learner(visitor)
      assert_equal(false, g1.too_big?(area_size))
    end
    for i in (4..5)
      visitor = User.new({name: "Visitor#{i}", email: "visitor#{i}@example.com", password: "password1"})
      g1.add_learner(visitor)
      assert_equal(false, g1.too_big?(area_size))
    end
    i = 6
    visitor = User.new({name: "Visitor#{i}", email: "visitor#{i}@example.com", password: "password1"})
    g1.add_learner(visitor)
    assert_equal(true, g1.too_big?(area_size))
  end

  def test_splittable?
    g1 = Tribe.new(1)
    i = 1
    visitor = User.new({name: "Visitor#{i}", email: "visitor#{i}@example.com", password: "password1"})
    g1.add_learner(visitor)
    assert_equal(false, g1.splittable?)
    i = 2
    visitor = User.new({name: "Visitor#{i}", email: "visitor#{i}@example.com", password: "password1"})
    g1.add_teacher(visitor)
    assert_equal(false, g1.splittable?)
    i = 3
    visitor = User.new({name: "Visitor#{i}", email: "visitor#{i}@example.com", password: "password1"})
    g1.add_learner(visitor)
    assert_equal(false, g1.splittable?)
    i = 4
    visitor = User.new({name: "Visitor#{i}", email: "visitor#{i}@example.com", password: "password1"})
    g1.add_teacher(visitor)
    assert_equal(true, g1.splittable?)
  end

  def test_split
    g1 = Tribe.new(1)
    for i in (1..3)
      visitor = User.new({name: "Visitor#{i}", email: "visitor#{i}@example.com", password: "password1"})
      g1.add_learner(visitor)
    end
    for i in (4..5)
      visitor = User.new({name: "Visitor#{i}", email: "visitor#{i}@example.com", password: "password1"})
      g1.add_teacher(visitor)
    end
    assert_equal(true, g1.splittable?)
    g2 = g1.split()
    assert_equal(g1.knowhow_id, g2.knowhow_id)
    assert_equal(1, g2.learner_count)
    assert_equal(1, g2.teacher_count)
    assert_equal(2, g1.learner_count)
    assert_equal(1, g1.teacher_count)
  end

end

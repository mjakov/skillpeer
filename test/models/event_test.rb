require 'test_helper'

class EventTest < ActiveSupport::TestCase

  test "correct meeting event" do
    mg = groups(:lacotabo)
    tStart = DateTime.parse("2099-08-30 18:00")
    tEnd = DateTime.parse("2099-08-30 21:00")
    me = Event.new(group_id: mg.id, time_start: tStart,
      time_end: tEnd)
    assert me.valid?
  end

  test "meeting event cannot be without corresponding meeting group" do
    tStart = DateTime.parse("2099-08-30 18:00")
    tEnd = DateTime.parse("2099-08-30 21:00")
    me = Event.new(group_id: nil, time_start: tStart,
      time_end: tEnd)
    assert_not me.valid?
  end

  test "time end cannot be before time start" do
    mg = groups(:lacotabo)
    tStart = DateTime.parse("2099-08-30 18:00")
    tEnd = DateTime.parse("2099-08-29 21:00")
    me = Event.new(group_id: mg.id, time_start: tStart,
      time_end: tEnd)
    assert_not me.valid?, "Meeting end time should not be before start time"
  end

  test "time start cannot be in the past" do
    mg = groups(:lacotabo)
    tStart = Date.yesterday.to_datetime
    tEnd = tStart + 2.hours
    me = Event.new(group_id: mg.id, time_start: tStart,
      time_end: tEnd)
    assert_not me.valid?, "The meeting event should not be created in the past"
  end

  test "should not be possible to create two meeting events with overlapping\
  times for the same meeting group 1" do

    mg = groups(:lacotabo)
    tStart = DateTime.parse("2099-08-30 18:00")
    tEnd = DateTime.parse("2099-08-30 21:00")
    me1 = Event.new(group_id: mg.id, time_start: tStart,
      time_end: tEnd)
    assert me1.valid?
    me1.save

    other_tStart = tStart - 1.hour
    other_tEnd = tStart + 1.hour
    me2 = Event.new(group_id: mg.id, time_start: other_tStart,
      time_end: other_tEnd)
    assert_not me2.valid?, "The second meeting event should not be valid " +
      "beacuse it overlaps with the first one"
  end

  test "should not be possible to create two meeting events with overlapping\
  times for the same meeting group 2" do

    mg = groups(:lacotabo)
    tStart = DateTime.parse("2099-08-30 18:00")
    tEnd = DateTime.parse("2099-08-30 21:00")
    me1 = Event.new(group_id: mg.id, time_start: tStart,
      time_end: tEnd)
    assert me1.valid?
    me1.save

    other_tStart = tStart + 1.hour
    other_tEnd = tEnd + 1.hour
    me2 = Event.new(group_id: mg.id, time_start: other_tStart,
      time_end: other_tEnd)
    assert_not me2.valid?, "The second meeting event should not be valid " +
      "beacuse it overlaps with the first one"
  end

  test "should not be possible to create two meeting events with overlapping\
  times for the same meeting group 3" do

    mg = groups(:lacotabo)
    tStart = DateTime.parse("2099-08-30 18:00")
    tEnd = DateTime.parse("2099-08-30 21:00")
    me1 = Event.new(group_id: mg.id, time_start: tStart,
      time_end: tEnd)
    assert me1.valid?
    me1.save

    other_tStart = tStart + 1.hour
    other_tEnd = tEnd - 1.hour
    me2 = Event.new(group_id: mg.id, time_start: other_tStart,
      time_end: other_tEnd)
    assert_not me2.valid?, "The second meeting event should not be valid " +
      "beacuse it overlaps with the first one"
  end

  test "should not be possible to create two meeting events with overlapping\
  times for the same meeting group 4" do

    mg1 = groups(:lacotabo)
    mg2 = groups(:babilon)
    tStart = DateTime.parse("2099-08-30 18:00")
    tEnd = DateTime.parse("2099-08-30 21:00")
    me1 = Event.new(group_id: mg1.id, time_start: tStart,
      time_end: tEnd)
    assert me1.valid?
    me1.save

    other_tStart = tStart + 1.hour
    other_tEnd = tEnd - 1.hour
    me2 = Event.new(group_id: mg2.id, time_start: other_tStart,
      time_end: other_tEnd)
    assert me2.valid?, "should be no overlap because other meeting group"
  end

  test "can add event to group" do
    mg1 = groups(:lacotabo)
    tStart = DateTime.parse("2099-08-30 18:00")
    tEnd = DateTime.parse("2099-08-30 21:00")
    assert_difference "Event.count", 2 do
      newEv = mg1.events.build(time_start: tStart, time_end: tEnd)
      newEv.save
      newEv = mg1.events.build(time_start: tEnd + 1.hours,
        time_end: tEnd + 2.hours)
      newEv.save
    end
  end

  test "can add visitor to event group" do
    ev = events(:lacotabo_sunday)
    usr1 = users(:marktwain)
    usr2 = users(:henryhudson)
    assert_difference "ev.users.count", 2 do
      ev.users<<usr1
      ev.users<<usr2
    end
  end

  test "recognize empty time given" do
    mg = groups(:lacotabo)
    ev = mg.events.build(time_start: "", time_end: "")
    assert_not ev.valid?
  end

  test "recognize wrong date time format given" do
    mg = groups(:lacotabo)
    ev = mg.events.build(time_start: "2099-08 08", time_end: "2099 08 08 20:00")
    assert_not ev.valid?
  end

  test "validate correct date time format 1" do
    mg = groups(:lacotabo)
    ev = mg.events.build(time_start: "2099-08-31 17:00", time_end: "2099-08-31 20:00")
    assert ev.valid?
  end

end#class

require 'test_helper'
require 'agenda/manager'

include Agenda

class TestManager < ActiveSupport::TestCase

  def setup
    area_size = 6
    area_count = 5
    @mgr = Manager.new(area_size, area_count)
    assert_equal(area_size, @mgr.area_size)
    assert_equal(area_count, @mgr.area_count)
    assert_equal(0, @mgr.visitor_count)
    assert_equal([], @mgr.visitor_list)
    assert_equal(0, @mgr.tribe_count)
    assert_equal([], @mgr.tribe_list)
  end

  def test_init
    assert_not_nil @mgr
  end

  def test_add_visitor
    v1 = create_visitor
    v2  = create_visitor
    vcnt = @mgr.visitor_count
    @mgr.add_visitor(v1)
    @mgr.add_visitor(v2)
    vcnt_new = @mgr.visitor_count
    assert_equal(vcnt + 2, vcnt_new)
    assert(@mgr.visitor_list.include?(v1))
    assert(@mgr.visitor_list.include?(v2))
  end

  def test_add_tribe
    gCnt = @mgr.tribe_count
    g1 = Tribe.new(1)
    g2 = Tribe.new(2)
    @mgr.send(:add_tribe, g1)
    @mgr.send(:add_tribe, g2)
    newgCnt = @mgr.tribe_count
    assert_equal(gCnt + 2, newgCnt)
    assert(@mgr.tribe_list.include?(g1))
    assert(@mgr.tribe_list.include?(g2))
  end

  def test_get_tribes_by_knowhow
    g2 = @mgr.send(:get_tribes_by_knowhow, 2)
    assert_equal(0, g2.length)
    for i in (1..5)
      @mgr.send(:add_tribe, Tribe.new(i))
    end
    g2 = @mgr.send(:get_tribes_by_knowhow, 2)
    assert_equal(1, g2.length)
    assert_equal(2, g2[0].knowhow_id)
    for i in (1..5)
      @mgr.send(:add_tribe, Tribe.new(i))
    end
    g2 = @mgr.send(:get_tribes_by_knowhow, 2)
    assert_equal(2, g2.length)
    assert_equal(2, g2[0].knowhow_id)
    assert_equal(2, g2[1].knowhow_id)
  end

  def test_form_tribes
    vArr = []
    for i in (0..2)
      vArr << create_visitor
    end
    vArr[0].teaches_by_id(1)
    vArr[0].learns_by_id(2)
    vArr[1].learns_by_id(1)
    vArr[1].teaches_by_id(2)
    vArr[2].teaches_by_id(1)
    vArr[2].teaches_by_id(2)
    for i in (0..2)
      @mgr.add_visitor(vArr[i])
    end
    assert_equal(3, @mgr.visitor_count)
    @mgr.send(:form_tribes)
    @mgr.send(:form_tribes)
    assert_equal(2, @mgr.tribe_count)
    g1 = @mgr.send(:get_tribes_by_knowhow, 1).first
    assert_equal(1, g1.knowhow_id)
    assert_equal(2, g1.teacher_count)
    assert_equal(1, g1.learner_count)
    g2 = @mgr.send(:get_tribes_by_knowhow, 2).first
    assert_equal(2, g2.knowhow_id)
    assert_equal(2, g2.teacher_count)
    assert_equal(1, g2.learner_count)
  end

  def test_eliminate_unfeasible_tribes_1
    vArr = []
    for i in (0..2)
      vArr << create_visitor
    end
    vArr[0].teaches_by_id(1)
    vArr[0].learns_by_id(2)
    vArr[1].teaches_by_id(1)
    vArr[1].learns_by_id(2)
    vArr[2].teaches_by_id(1)
    vArr[2].learns_by_id(2)
    for i in (0..2)
      @mgr.add_visitor(vArr[i])
    end
    assert_equal(3, @mgr.visitor_count)
    @mgr.send(:form_tribes)
    assert_equal(2, @mgr.tribe_count)
    @mgr.send(:eliminate_unfeasible_tribes)
    assert_equal(0, @mgr.tribe_count)
  end

  def test_eliminate_unfeasible_tribes_2
    vArr = []
    for i in (0..2)
      vArr << create_visitor
    end
    vArr[0].learns_by_id(1)
    vArr[0].learns_by_id(2)
    vArr[1].teaches_by_id(1)
    vArr[1].learns_by_id(2)
    vArr[2].teaches_by_id(1)
    vArr[2].learns_by_id(2)
    for i in (0..2)
      @mgr.add_visitor(vArr[i])
    end
    assert_equal(3, @mgr.visitor_count)
    @mgr.send(:form_tribes)
    assert_equal(2, @mgr.tribe_count)
    @mgr.send(:eliminate_unfeasible_tribes)
    assert_equal(1, @mgr.tribe_count)
    assert_equal(1, @mgr.send(:get_tribes_by_knowhow, 1).length)
    assert_equal(0, @mgr.send(:get_tribes_by_knowhow, 2).length)
  end

  def test_eliminate_unfeasible_tribes_3
    vArr = []
    for i in (0..2)
      vArr << create_visitor
    end
    vArr[0].learns_by_id(1)
    vArr[0].learns_by_id(2)
    vArr[1].teaches_by_id(1)
    vArr[1].learns_by_id(2)
    vArr[2].teaches_by_id(1)
    vArr[2].teaches_by_id(2)
    for i in (0..2)
      @mgr.add_visitor(vArr[i])
    end
    assert_equal(3, @mgr.visitor_count)
    @mgr.send(:form_tribes)
    assert_equal(2, @mgr.tribe_count)
    @mgr.send(:eliminate_unfeasible_tribes)
    assert_equal(2, @mgr.tribe_count)
  end

  def test_too_big_tribes
    gpL = []
    gpL << Tribe.new(1) << Tribe.new(2) << Tribe.new(3)
    for g in gpL
      @mgr.send(:add_tribe, g)
    end
    assert_equal(false, @mgr.send(:too_big_tribes?))
    for i in (1..4)
      v = create_visitor
      for g in gpL
        g.add_learner(v)
      end
    end
    assert_equal(false, @mgr.send(:too_big_tribes?))
    for i in (5..7)
      v = create_visitor
      for g in gpL
        g.add_learner(v)
      end
    end
    assert_equal(true, @mgr.send(:too_big_tribes?))
  end

  def test_splitG_1
    gpL = []
    aSz = @mgr.area_size
    assert_equal([], Manager.splitG(gpL, aSz))
    gpL << Tribe.new(1)
    assert_equal(gpL, Manager.splitG(gpL, aSz))
    for i in (1..3)
      v = create_visitor
      for g in gpL
        g.add_learner(v)
      end
    end
    for i in (4..(aSz + 1))
      v = create_visitor
      for g in gpL
        g.add_learner(v)
      end
    end
    newGpL = Manager.splitG(gpL, aSz)
    assert_equal(2, newGpL.length)
  end

  def test_splitG_2
    gpL = []
    aSz = @mgr.area_size
    assert_equal([], Manager.splitG(gpL, aSz))
    gpL << Tribe.new(1) << Tribe.new(2)
    spGpL = Manager.splitG(gpL, aSz)
    assert_equal(2, gpL.length)
    assert_equal(2, spGpL.length)
    assert_equal(gpL, spGpL)
    for i in (1..3)
      v = create_visitor
      for g in gpL
        g.add_learner(v)
      end
    end
    for i in (4..(aSz + 1))
      v = create_visitor
      for g in gpL
        g.add_learner(v)
      end
    end
    gpL << Tribe.new(3)
    newGpL = Manager.splitG(gpL, aSz)
    assert_equal(5, newGpL.length)
  end

  def test_split_too_big_tribes_1
    gpL = []
    gpL << Tribe.new(1) << Tribe.new(2) << Tribe.new(3)
    for i in (1..4)
      v = create_visitor
      for g in gpL
        g.add_learner(v)
      end
    end
    for i in (5..7)
      v = create_visitor
      for g in gpL
        g.add_learner(v)
      end
    end
    for g in gpL
      @mgr.send(:add_tribe, g)
    end
    assert_equal(true, @mgr.send(:too_big_tribes?))
    assert_equal(3, @mgr.tribe_count)
    @mgr.send(:split_too_big_tribes)
    assert_equal(false, @mgr.send(:too_big_tribes?))
    assert_equal(6, @mgr.tribe_count)
  end

  def test_split_too_big_tribes_2
    gpL = []
    gpL << Tribe.new(1) << Tribe.new(2)
    for i in (1..4)
      v = create_visitor
      for g in gpL
        g.add_learner(v)
      end
    end
    for i in (5..(2 * (@mgr.area_size + 1)))
      v = create_visitor
      for g in gpL
        g.add_learner(v)
      end
    end
    gpL << Tribe.new(3)
    for g in gpL
      @mgr.send(:add_tribe, g)
    end
    assert_equal(true, @mgr.send(:too_big_tribes?))
    assert_equal(3, @mgr.tribe_count)
    @mgr.send(:split_too_big_tribes)
    assert_equal(false, @mgr.send(:too_big_tribes?))
    assert_equal(9, @mgr.tribe_count)
  end

  def test_calc_min_rounds
    assert_equal(0, @mgr.send(:calc_min_rounds))
    @mgr.area_count = 5
    for i in (1..7)
      @mgr.send(:add_tribe, Tribe.new(i))
    end
    assert_equal(2, @mgr.send(:calc_min_rounds))
    @mgr.area_count = 4
    assert_equal(2, @mgr.send(:calc_min_rounds))
    @mgr.area_count = 3
    assert_equal(3, @mgr.send(:calc_min_rounds))
  end

  # Tests that only splittable tribes will be split once
  def test_split_additional_tribes_1
    gpL = []
    g1 = Tribe.new(1)
    g2 = Tribe.new(2)
    g3 = Tribe.new(3)
    gpL << g1 << g2 << g3
    for g in gpL
      @mgr.send(:add_tribe, g)
    end
    for i in (1..3)
      v = create_visitor
      g1.add_learner(v)
      g2.add_learner(v)
    end
    g3.add_learner(create_visitor)
    for i in (3..@mgr.area_size)
      v = create_visitor
      g2.add_teacher(v)
      g3.add_teacher(v)
    end
    g1.add_teacher(create_visitor)
    assert_equal(true, @mgr.tribe_list.include?(g1))
    assert_equal(true, @mgr.tribe_list.include?(g2))
    assert_equal(true, @mgr.tribe_list.include?(g3))
    @mgr.send(:split_additional_tribes, 6)
    assert_equal(4, @mgr.tribe_count)
    assert_equal(true, @mgr.tribe_list.include?(g1))
    assert_equal(true, @mgr.tribe_list.include?(g3))
  end

  # Tests that tribes are split only up to slot count
  def test_split_additional_tribes_2
    gpL = []
    g1 = Tribe.new(1)
    g2 = Tribe.new(2)
    g3 = Tribe.new(3)
    gpL << g1 << g2 << g3
    for g in gpL
      @mgr.send(:add_tribe, g)
    end
    for i in (1..3)
      v = create_visitor
      g1.add_learner(v)
      g2.add_learner(v)
      g3.add_learner(v)
    end
    for i in (3..@mgr.area_size)
      v = create_visitor
      g1.add_teacher(v)
      g2.add_teacher(v)
      g3.add_teacher(v)
    end
    g1.add_learner(create_visitor)
    g2.add_teacher(create_visitor)
    assert_equal(3, @mgr.tribe_count)
    @mgr.send(:split_additional_tribes, 5)
    assert_equal(5, @mgr.tribe_count)
    g3Nw = @mgr.tribe_list[@mgr.tribe_list.index(g3)]
    assert_equal(3, g3Nw.learner_count)
    g2Nw = @mgr.tribe_list[@mgr.tribe_list.index(g2)]
    assert (g2Nw.learner_count != 3)
    g1Nw = @mgr.tribe_list[@mgr.tribe_list.index(g1)]
    assert (g1Nw.learner_count != 4)
  end

  def test_reset_tribes
    mgr = Manager.new(3,2)
    gpL = []
    g1 = Tribe.new(1)
    g2 = Tribe.new(2)
    g3 = Tribe.new(3)
    gpL << g1 << g2 << g3
    for g in gpL
      mgr.send(:add_tribe, g)
    end
    assert_equal(gpL.length, mgr.tribe_count)
    mgr.send(:reset_tribes)
    assert(mgr.tribe_list.empty?)
    assert_equal(0, mgr.tribe_count)
  end

  def test_make_schedule_1
    area_size = 3
    area_count = 1
    mgr = Manager.new(area_size, area_count)
    visitors = []
    for i in (0..2)
      visitors << create_visitor
    end
    visitors[0].learns_by_id(0)
    visitors[1].learns_by_id(0)
    visitors[2].teaches_by_id(0)
    visitors[0].teaches_by_id(1)
    visitors[1].learns_by_id(1)
    visitors[2].learns_by_id(1)

    for vis in visitors
      mgr.add_visitor(vis)
    end

    schedule = mgr.make_schedule

    assert_equal(2, schedule.length)
    assert( (schedule[0][0].knowhow_id == 0 && schedule[1][0].knowhow_id == 1) ||
            (schedule[0][0].knowhow_id == 1 && schedule[1][0].knowhow_id == 0))
  end #test_make_schedule_1

  def test_make_schedule_2
    area_size = 3
    area_count = 2
    mgr = Manager.new(area_size, area_count)
    visitors = []
    for i in (0..5)
      visitors << create_visitor
    end

    visitors[0].learns_by_id(5)
    visitors[1].learns_by_id(5)
    visitors[2].learns_by_id(5)
    visitors[3].learns_by_id(5)
    visitors[5].teaches_by_id(5)

    visitors[0].teaches_by_id(1)
    visitors[2].learns_by_id(1)
    visitors[3].learns_by_id(1)
    visitors[4].learns_by_id(1)
    visitors[5].teaches_by_id(1)

    for vis in visitors
      mgr.add_visitor(vis)
    end

    schedule = mgr.make_schedule
    #puts Optimizer::schedule_inspect(schedule)
    assert_equal(2, schedule.length)

    kwIdSum = 0

    for rd in schedule
      for gp in rd
        kwIdSum += gp.knowhow_id
      end
    end

    assert_equal(12, kwIdSum)

  end #test_make_schedule_2

  # Tests unfeasible tribes
  def test_make_schedule_3
    area_size = 3
    area_count = 2
    mgr = Manager.new(area_size, area_count)
    visitors = []
    for i in (0..5)
      visitors << create_visitor
    end

    visitors[0].learns_by_id(5)
    visitors[1].learns_by_id(5)
    visitors[2].learns_by_id(5)
    visitors[3].learns_by_id(5)
    visitors[5].teaches_by_id(5)

    visitors[0].teaches_by_id(1)
    visitors[2].learns_by_id(1)
    visitors[3].learns_by_id(1)
    visitors[4].learns_by_id(1)
    visitors[5].teaches_by_id(1)

    visitors[0].learns_by_id(3)
    visitors[1].learns_by_id(3)

    for vis in visitors
      mgr.add_visitor(vis)
    end

    schedule = mgr.make_schedule
    #puts Optimizer::schedule_inspect(schedule)
    assert_equal(2, schedule.length)

    kwIdSum = 0

    for rd in schedule
      for gp in rd
        kwIdSum += gp.knowhow_id
      end
    end

    assert_equal(12, kwIdSum)

  end #test_make_schedule_3

end

require 'test_helper'

class GroupTest < ActiveSupport::TestCase

  def setup
    @facilitator = users(:tomsawyer)
    @nameStr = "Open Language Conversation Table"
    @locationStr = "Pivnica Pinta"
    @addressStr = "Radiceva 3, 10000 Zagreb, Croatia"
    @maxPlacesInt = 30
    @descriptionStr = "Group meets every Sunday at 5 P.M.\n Free entry"
    @area_size = 6
    @area_count = 5

    @meetingGp = Group.new(name: @nameStr, max_places: @maxPlacesInt,
      description: @descriptionStr, facilitator_id: @facilitator.id,
      area_size: @area_size, area_count: @area_count)
  end

  def test_init
    assert_equal(@nameStr, @meetingGp.name)
    assert_equal(@meetingGp.location.name, "Default")
    assert_equal(@maxPlacesInt, @meetingGp.max_places)
    assert_equal(@descriptionStr, @meetingGp.description)
    assert_equal(@facilitator.id, @meetingGp.facilitator_id)
    assert_equal(@area_size, @meetingGp.area_size)
    assert_equal(@area_count, @meetingGp.area_count)
  end

  test "max places should be greater than zero" do
    meetingGp = Group.new(name: @nameStr, max_places: -1, description: @descriptionStr,
      facilitator_id: @facilitator.id, area_size: 6, area_count: 5)

    assert_not(meetingGp.valid?)
  end

  test "max places should be an integer" do
    meetingGp = Group.new(name: @nameStr, max_places: 1.01,
      description: @descriptionStr,
      facilitator_id: @facilitator.id, area_size: 6, area_count: 5)

    assert_not(meetingGp.valid?)
  end

  test "missing description should be fine" do
    meetingGp = Group.new(name: @nameStr,
      max_places: @maxPlacesInt, description: "",
      facilitator_id: @facilitator.id, area_size: 6, area_count: 5)

    assert(meetingGp.valid?)
  end

  test "too long description should not be valid" do
    meetingGp = Group.new(name: @nameStr, max_places: @maxPlacesInt, description: "a"*1001,
      facilitator_id: @facilitator.id, area_size: 6, area_count: 5)

    assert_not(meetingGp.valid?)
  end

  test "name and location together should be unique" do
    meetingGp = Group.new(name: @nameStr, max_places: 30, description: "a"*500,
      facilitator_id: @facilitator.id, area_size: 6, area_count: 5
      )
    meetingGp.location = locations(:pinta)
    assert(meetingGp.valid?, "group should be valid")
    meetingGp.save

    meetingGpOther = Group.new(name: @nameStr,
      max_places: 31, description: "a"*1000,
      facilitator_id: @facilitator.id, area_size: 6, area_count: 5
      )
    assert(meetingGpOther.valid?, "group should be valid")
    meetingGpOther.location = locations(:pinta)
    assert_not(meetingGpOther.valid?, "group should not be valid any more")
  end

  test "the event with the same name should be able to take place at different locations" do
    meetingGp = Group.new(name: @nameStr, location: locations(:pinta),
      max_places: 30, description: "a"*500,
      facilitator_id: @facilitator.id, area_size: 6, area_count: 5)

    assert(meetingGp.valid?, "group should be valid")

    meetingGpOther = Group.new(name: @nameStr,
      location: locations(:zlatnimedo), max_places: 31, description: "a"*1000,
      facilitator_id: @facilitator.id, area_size: 6, area_count: 5)

    assert(meetingGpOther.valid?, "group should be valid")
    assert(meetingGp.save)
    assert(meetingGpOther.valid?, "group should be valid")
  end

  test "facilitator id can't be negative" do
    meetingGp = Group.new(name: @nameStr, max_places: 30, description: "a"*500,
      facilitator_id: -1, area_size: 6, area_count: 5
      )
    assert_not(meetingGp.valid?, "Meeting group should not be valid")
  end

  test "facilitator id can't be non-integer" do
    meetingGp = Group.new(name: @nameStr, max_places: 30, description: "a"*500,
      facilitator_id: 1.5, area_size: 6, area_count: 5
      )
    assert_not(meetingGp.valid?, "Meeting group should not be valid")
  end

  test "Meeting group should have a corresponding facilitator" do

    meetingGp = Group.new(name: @nameStr, max_places: 31, description: "a"*1000,
      facilitator_id: @facilitator.id, area_size: 6, area_count: 5)

    assert_equal(@facilitator, meetingGp.facilitator)

  end

  test "A meeting group can be created through the user" do

    facilitator = users(:tomsawyer)
    mg = facilitator.groups.build(name: @nameStr,
      max_places: 31, description: "a"*1000,
      area_size: 6, area_count: 5)

    assert_equal(facilitator, mg.facilitator)
    assert(mg.valid?)

    assert_difference("Group.count", 1) do
      mg.save
    end

  end#test

  test "A group can't have zero area_size" do
    meetingGp = Group.new(name: @nameStr, max_places: 31, description: "a"*1000,
      facilitator_id: @facilitator.id, area_size: 0, area_count: 5)
    assert_not(meetingGp.valid?)
  end

  test "A group's area_size has to be an integer" do
    meetingGp = Group.new(name: @nameStr, max_places: 31, description: "a"*1000,
      facilitator_id: @facilitator.id, area_size: 6.1, area_count: 5)
    assert_not(meetingGp.valid?)
  end

  test "A group can't have zero area_count" do
    meetingGp = Group.new(name: @nameStr, max_places: 31, description: "a"*1000,
      facilitator_id: @facilitator.id, area_size: 6, area_count: 0)
    assert_not(meetingGp.valid?)
  end

  test "A group's area_count has to be an integer" do
    meetingGp = Group.new(name: @nameStr, max_places: 31, description: "a"*1000,
      facilitator_id: @facilitator.id, area_size: 6, area_count: 5.1)
    assert_not(meetingGp.valid?)
  end

end

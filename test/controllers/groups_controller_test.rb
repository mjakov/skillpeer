require 'test_helper'

class GroupsControllerTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  def setup
    @mg_name = "Cafe CKT"
    @mg_location = Location.create(name: "Centar Mladih Tresnjevka",
      country: "Croatia", postnr: 10000, city: "Zagreb",
      street: "Tresnjevacki trg", number: "20");
    @mg_max_places = 20
    @mg_description = "Everyone is welcome."
  end

  test "there should be a correct route for the create group action" do
    assert_recognizes({controller: 'groups', action: 'create'} ,
      { path: 'groups', method: :post})
  end

  test "there should be a correct route for the new group action" do
    assert_recognizes({controller: 'groups', action: 'new'} ,
      { path: 'groups/new', method: :get})
  end

  test "meeting group should not be created if no user is not logged in" do
    assert_no_difference 'Group.count' do
      post groups_path, params: { group: { name: @mg_name, max_places: @mg_max_places,
        description: @mg_description, area_size: 6, area_count: 5 } }
    end
    assert_redirected_to root_url
  end

  test "meeting group should not be created if a user does not have a facilitator role" do
    visitor = users(:marktwain)
    login_as visitor
    follow_redirect!
    assert_no_difference 'Group.count' do
      post groups_path, params: { group: { name: @mg_name,
        max_places: @mg_max_places,
        description: @mg_description, area_size: 6, area_count: 5 } }
    end
    assert_not(flash[:danger], "There should not be any message")
    assert_not(flash[:success], "There should not be any message")
    assert_not(flash[:info], "There should not be any message")
  end

  test "meeting group for a facilitator user, without a name should not be created" do
    facUsr = users(:tomsawyer)
    login_as facUsr
    follow_redirect!
    assert_no_difference 'Group.count' do
      post groups_path, params: { group: { name: "",
        max_places: @mg_max_places,
        description: @mg_description, area_size: 6, area_count: 5 } }
    end
    assert(flash[:danger], "There should be a general flash error message")

    assert_select("li", /Name/) do
      assert_select("[class=?]", "error_item")
    end

  end#test

  test "meeting group should be created for a facilitator user" do
    facilitator = users(:tomsawyer)
    login_as facilitator
    follow_redirect!
    assert_difference('Group.count', 1) do
      post groups_path, params: { group: { name: @mg_name,
        max_places: @mg_max_places,
        description: @mg_description, area_size: 6, area_count: 5 } }
    end
    assert_redirected_to root_url
    assert(flash[:success], "There should be a success message")
  end

  test "meeting group with incorrect area_size can't be created for facilitator" do
    facilitator = users(:tomsawyer)
    login_as facilitator
    follow_redirect!
    assert_no_difference('Group.count') do
      post groups_path, params: { group: { name: @mg_name,
        max_places: @mg_max_places,
        description: @mg_description, area_size: 1, area_count: 5 } }
    end
    assert_template :new
    assert(flash[:danger], "There should be an error message")
  end

  test "meeting group with missing area_count can't be created for facilitator" do
    facilitator = users(:tomsawyer)
    login_as facilitator
    follow_redirect!
    assert_no_difference('Group.count') do
      post groups_path, params: { group: { name: @mg_name,
        max_places: @mg_max_places,
        description: @mg_description, area_size: 6 } }
    end
    assert_template :new
    assert(flash[:danger], "There should be an error message")
  end

  # Template tests
  # Link to new meeting group page tests
  test "non logged in user should not have a link to create a meeting group" do
    get root_url
    assert_response :success
    assert_select "a[href=?]", new_group_path, { count: 0 }
  end

  test "non logged in user should not be able to access the new meeting group page" do
    get new_group_path
    assert_redirected_to root_url
  end

  test "logged in non-facilitator user should not have a link to create a meeting group" do
    get root_url
    assert_response :success
    visitor = users(:marktwain)
    login_as visitor
    assert_redirected_to root_url
    follow_redirect!
    assert_select "a[href=?]", new_group_path, { count: 0 }
  end

  test "non-facilitator user should not have a link to create a meeting group" do
    visitor = users(:marktwain)
    login_as visitor
    get new_group_path
    assert_redirected_to root_url
  end

  test "the content of the new meeting group page for the facilitator user" do
    facilitator = users(:tomsawyer)
    login_as facilitator
    assert_redirected_to root_url
    follow_redirect!
    get new_group_path
    assert_response :success
    assert_select "form[action=?]", groups_path do
      assert_select "[method=?]", 'post'
    end
    assert_select "input[type=?]", "submit"

    # Page title
    assert_select("title", { content: "New Meeting Group"})

    # Form fields
    assert_select "input[name=?]", "group[name]" do
      assert_select "[type=?]", "text"
    end
    assert_select "input[name=?]", "group[location]", false
    assert_select "input[name=?]", "group[address]", false
    assert_select "input[name=?]", "group[max_places]" do
      assert_select "[type=?]", "number"
    end
    assert_select "textarea[name=?]", "group[description]"
    assert_select "input[name=?]", "group[facilitator_id]", { count: 0 }

    assert_select "a[href=?]", root_path
  end#test

  test "#meeting group show page should be accessible by anyone" do
    mg = groups(:lacotabo)
    get group_path(mg)
    assert_response :success
    assert_select("h2", mg.name)
    assert_select("p", { text: mg.description } )
    assert_select("p", { text: /#{mg.location.name}/ })
    assert_select("p", { text: /#{mg.location.street}/ })
    assert_select("td", mg.max_places.to_s)

    assert_select("p", { text: /Tom Sawyer/, count: 0})
    assert_select("body", { text: /facilitator/, count: 0})
    assert_select("body", { text: /Facilitator/, count: 0})
  end

  test "#index page should be accessible by anyone and show a list of\
        meeting groups" do

    get groups_path
    assert_response :success
    assert_select '*', "Groups"
    Group.take(5).each do |grp|
      assert_select "*", /#{grp.name}/
    end

    for mg in Group.all
      assert_select "li" do
        assert_select "a[href=?]", group_path(mg)
      end
    end
  end

  test "index view for the facilitator should only show his meeting groups" do
    facilitator = users(:tomsawyer)
    login_as facilitator
    get groups_path
    facilitator.groups.each do |grp|
      assert_select "a", /#{grp.name}/
      assert_select "*", /#{grp.name}/
    end

    logout

    facilitator = users(:facilitator2)
    login_as facilitator
    get groups_path
    facilitator.groups.each do |grp|
      assert_select "*", /#{grp.name}/
    end
    logout
  end

  test "meeting groups index should be linked from the home page" do
    get root_url
    assert_response :success
    assert_select "a[href=?]", groups_path
  end

  ######################
  # Test Update Action #
  ######################

  test "update route recognized" do
    assert_recognizes({ controller: 'groups', action: 'update', id: "1" },
      { path: '/groups/1', method: 'patch' })
  end

  test "update action from non logged in user should not be accepted" do
    mg = groups(:lacotabo)

    patch group_path(mg), params: { group: {name: "New Meeting Group",
      max_places: mg.max_places,
      description: mg.description, area_size: 6, area_count: 5} }

    mgOtr = Group.find(mg.id)
    assert_equal(mg.name, mgOtr.name)

    assert_redirected_to root_url
  end

  test "update action from non facilitator user should not be accepted" do

    mg = groups(:lacotabo)
    visitor = users(:marktwain)
    login_as visitor
    follow_redirect!

    patch group_path(mg), params: { group: {name: "New Meeting Group",
      max_places: mg.max_places,
      description: mg.description, area_size: 6, area_count: 5 } }

    mgOtr = Group.find(mg.id)
    assert_equal(mg.name, mgOtr.name)

    assert_redirected_to root_url

  end

  test "legal update action from facilitator user" do

    mg = groups(:lacotabo)
    facilitator = users(:tomsawyer)
    login_as facilitator
    follow_redirect!

    newNmStr = "New Language Conversation Group"

    patch group_path(mg), params: { group: {name: newNmStr,
      max_places: mg.max_places,
      description: mg.description, area_size: 6, area_count: 5} }

    mgOtr = Group.find(mg.id)
    assert_equal(newNmStr, mgOtr.name)

    assert_redirected_to group_path(mg)

  end

  test "facilitator should not be able to update a group that does not belong to him" do

    mg = groups(:lacotabo)
    facilitator = users(:facilitator2)
    login_as facilitator
    follow_redirect!

    newNmStr = "New Language Conversation Group"

    patch group_path(mg), params: { group: {name: newNmStr,
      max_places: mg.max_places,
      description: mg.description, area_size: 6, area_count: 5 } }

    mgOtr = Group.find(mg.id)
    assert_not_equal(newNmStr, mgOtr.name)

    assert_redirected_to root_url

  end

  test "facilitator should not be able to update his group with bad info" do
    mg = groups(:lacotabo)
    facilitator = users(:tomsawyer)
    login_as facilitator
    follow_redirect!

    newNmStr = "A"
    oldNmStr = mg.name

    patch group_path(mg), params: { group: {name: newNmStr,
      max_places: mg.max_places,
      description: mg.description, area_size: 6, area_count: 5 } }

    mgOtr = Group.find(mg.id)
    assert_equal(oldNmStr, mgOtr.name)

    #assert_redirected_to groups_path
    assert_template :edit
  end#test

  test "facilitator should not be able to update his group with bad area_count" do
    mg = groups(:lacotabo)
    facilitator = users(:tomsawyer)
    login_as facilitator
    follow_redirect!

    oldAreaCount = mg.area_count

    patch group_path(mg), params: { group: {name: mg.name,
      max_places: mg.max_places,
      description: mg.description, area_size: 6, area_count: 10.0 } }

    mgOtr = Group.find(mg.id)
    assert_equal(oldAreaCount, mgOtr.area_count)

    #assert_redirected_to groups_path

    assert_template :edit
  end#test

  ######################
  # Test edit template #
  ######################

  test "the edit page should only be accessible by facilitator whose \
    meeting group it is" do

    facilitator = users(:tomsawyer)
    facOtr = users(:facilitator2)
    visitor = users(:marktwain)
    mg = facilitator.groups.first

    get edit_group_path(mg)
    assert_redirected_to root_url

    login_as(visitor)
    get edit_group_path(mg)
    assert_redirected_to root_url
    logout

    login_as(facOtr)
    get edit_group_path(mg)
    assert_redirected_to root_url
    logout

  end

  test "the content of edit meeting group page for the facilitator user" do
    facilitator = users(:tomsawyer)
    mg = facilitator.groups.first
    login_as facilitator
    assert_redirected_to root_url
    follow_redirect!
    get edit_group_path(mg)
    assert_response :success
    assert_select "form[action=?]", group_path(mg) do
      assert_select "[method=?]", 'post'
      assert_select "input" do
        assert_select "[type=?]", "hidden"
        assert_select "[value=?]", "patch"
      end
    end
    assert_select "input[type=?]", "submit"

    # Page title
    assert_select("title", { content: "Edit Meeting Group"})

    # Form fields
    assert_select "input[name=?]", "group[name]" do
      assert_select "[type=?]", "text"
    end
    assert_select "input[name=?]", "group[location]", false
    assert_select "input[name=?]", "group[address]", false
    assert_select "input[name=?]", "group[max_places]" do
      assert_select "[type=?]", "number"
    end
    assert_select "textarea[name=?]", "group[description]"
    assert_select "input[name=?]", "group[facilitator_id]", { count: 0 }

    assert_select "a[href=?]", groups_path
  end#test

  # Links to edit page

  test "Meeting group edit page link not displayed to non logged in users" do
    mg = groups(:lacotabo)
    get group_path(mg)
    assert_response :success
    assert_select "a[href=?]", edit_group_path(mg), { count: 0}
  end

  test "Meeting group edit page link not displayed to visitor users" do
    visitor = users(:marktwain)
    mg = groups(:lacotabo)
    login_as(visitor)
    get group_path(mg)
    assert_response :success
    assert_select "a[href=?]", edit_group_path(mg), { count: 0}
  end

  test "Meeting group edit page link not displayed to non-owning facilitator users" do
    facOtr = users(:facilitator2)
    mg = groups(:lacotabo)
    login_as(facOtr)
    get group_path(mg)
    assert_response :success
    assert_select "a[href=?]", edit_group_path(mg), { count: 0}
  end

  test "Meeting group edit page link is displayed to owning facilitator users" do
    facOtr = users(:tomsawyer)
    mg = groups(:lacotabo)
    login_as(facOtr)
    get group_path(mg)
    assert_response :success
    assert_select "a[href=?]", edit_group_path(mg)
  end

  # Links from edit page
  test "links from edit page" do
    facilitator = users(:tomsawyer)
    login_as facilitator
    mg = groups(:lacotabo)
    get edit_group_path(mg)
    assert_response :success
    assert_select "a[href=?]", groups_path
  end

  ################################
  ### Test Delete              ###
  ################################

  # Routes
  test "delete route recognized" do
    assert_recognizes({ controller: 'groups', action: 'destroy', id: "1" },
      { path: '/groups/1', method: 'delete' })
  end

  # Links
  test "there should not be a delete button on the show page for a non-logged in user" do
    mg = groups(:lacotabo)
    get group_path(mg)
    assert_response :success
    assert_select("form[action=?]", group_path(mg), { count: 0 }) do
      assert_select("[method=?]", 'delete', { count: 0})
    end
  end

  test "there should not be a delete button on the show page for a visitor user" do
    visitor = users(:marktwain)
    login_as visitor
    mg = groups(:lacotabo)
    get group_path(mg)
    assert_response :success
    assert_select("form[action=?]", group_path(mg), { count: 0 }) do
      assert_select("[method=?]", 'delete', { count: 0})
    end
  end

  test "there should not be a delete button on the show page for a non-owning facilitator" do
    facOtr = users(:facilitator2)
    login_as facOtr
    mg = groups(:lacotabo)
    get group_path(mg)
    assert_response :success
    assert_select("form[action=?]", group_path(mg), { count: 0 }) do
      assert_select("[method=?]", 'delete', { count: 0})
    end
  end

  test "there should be a delete button on the show page for the owning facilitator" do
    facilitator = users(:tomsawyer)
    login_as facilitator
    mg = groups(:lacotabo)
    get group_path(mg)
    assert_response :success
    assert_select("form[action=?]", group_path(mg), { count: 1 }) do
      assert_select("input[value=?]", 'delete', { count: 1})
    end
  end

  # Action
  test "a non-logged in user can't delete a meeting group" do
    mg = groups(:lacotabo)
    assert_no_difference "Group.count" do
      delete group_path(mg)
    end
    assert_redirected_to root_url
  end

  test "a visitor can't delete a meeting group" do
    visitor = users(:marktwain)
    login_as visitor
    mg = groups(:lacotabo)
    assert_no_difference "Group.count" do
      delete group_path(mg)
    end
    assert_redirected_to root_url
  end

  test "a non-owning facilitator can't delete a meeting group" do
    facOtr = users(:facilitator2)
    login_as facOtr
    mg = groups(:lacotabo)
    assert_no_difference "Group.count" do
      delete group_path(mg)
    end
    assert_redirected_to root_url
  end

  test "the owning facilitator can delete his meeting group" do
    facilitator = users(:tomsawyer)
    login_as facilitator
    mg = groups(:lacotabo)
    assert_difference "Group.count", -1  do
      delete group_path(mg)
    end
    assert(flash[:success])
    assert_redirected_to groups_path
  end

end#class

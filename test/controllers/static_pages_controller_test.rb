require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  test "should get home page" do
    get static_pages_home_url
    assert_response :success
    # Assert that there is a link to the registration page
    assert_select 'a[href=?]', new_user_path
    # Assert there are no errors
    assert_select '.flash_messages p', { count: 0 }
  end

  test "home page of logged in user should display his role" do

  end

end

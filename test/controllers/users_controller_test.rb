require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest

  test "should get registration page" do
    get new_user_path
    assert_response :success
    # Assert that we are on the home page
    assert_select "title", /Sign up/
    # Assert that there is a registration form
    assert_select "form", {count: 1}
    assert_select "form[id=?]", "new_user", {count: 1}

    assert_select "form label[for=?]", 'user_name', {count: 1}
    assert_select "form input[id=?]", 'user_name', {count: 1}
    assert_select "form input[name=?]", 'user[name]', {count: 1}

    assert_select "form label[for=?]", 'user_email', {count: 1}
    assert_select "form input[id=?]", 'user_email', {count: 1}
    assert_select "form input[name=?]", 'user[email]', {count: 1}

    assert_select "form label[for=?]", 'user_password', {count: 1}
    assert_select "form input[id=?]", 'user_password', {count: 1} do
      assert_select '[type=?]', 'password'
      assert_select "[name=?]", 'user[password]'
    end

    assert_select "form input[type=?]", 'submit', {count: 1}
  end #test

  test "missing parameters to post should not change database" do
    get new_user_path
    assert_response :success
    usrCnt = User.count
    post users_path, params: { user: { name: "Thomas Jefferson",
      email: "jefferson@example.com", password: "" } }
    usrCntNw = User.count
    assert_equal(usrCnt, usrCntNw)
    assert_equal 'Please correct your registration data', flash[:danger]
    # Check that the flash message is displayed
    assert_select '.flash_messages .danger'
  end

  test "correct parameters to post should change database" do
    get new_user_path
    assert_response :success
    usrCnt = User.count
    post users_path, params: { user: { name: "Thomas Jefferson",
      email: "jefferson@example.com", password: "somepassword1" } }
    usr = User.find_by(name: "Thomas Jefferson")
    assert_redirected_to user_path(usr)
    usrCntNw = User.count
    assert_equal(usrCnt + 1, usrCntNw)
    assert_match /Thomas/, flash[:success]

    # Check that the flash message is displayed
    follow_redirect!
    assert_select '.flash_messages .success'
  end #test

  test "user can't register twice with the same email" do
    email_str = "jefferson@example.com"
    password_str = "somepassword1"

    get new_user_path
    assert_response :success
    post users_path, params: { user: { name: "Thomas Jefferson",
      email: email_str, password: password_str } }
    follow_redirect!
    assert_select "body", /Thomas Jefferson/
    logout


    get new_user_path
    assert_response :success
    post users_path, params: { user: { name: "Marc Faber",
      email: email_str, password: password_str } }
    assert_select '.flash_messages .danger'
  end #test


  test "registration form should contain an error message area" do
    get new_user_path
    assert_response :success
    assert_select 'form .error_messages'
  end

  test "missing password should cause error messages" do
    get new_user_path
    assert_response :success
    post users_path, params: { user: { name: "Thomas Jefferson",
      email: "jefferson@example.com", password: "" } }
    assert_select '.error_messages' do
      assert_select 'li', /Password/, {count: 2}
    end
  end

  test "missing name should cause error messages" do
    get new_user_path
    assert_response :success
    post users_path, params: { user: { name: "",
      email: "jefferson@example.com", password: "somepassword1" } }
    assert_select '.error_messages' do
      assert_select 'li', /Name/, {count: 2}
    end
  end

  test "wrong email should cause error messages" do
    get new_user_path
    assert_response :success
    post users_path, params: { user: { name: "Thomas Jefferson",
      email: "jefferson@example dot com", password: "somepassword1" } }
    assert_select '.error_messages' do
      assert_select 'li', /Email/
    end
  end

  test "should be no stray messages at root" do
    get new_user_path
    assert_response :success
    post users_path, params: { user: { name: "Thomas Jefferson",
      email: "jefferson@example.com", password: "" } }
    get root_url
    assert_response :success
    # Assert there are no errors
    assert_select '.flash_messages p', { count: 0 }
  end

  test "profile page" do
    user = users(:marktwain)
    login_as(user)
    follow_redirect!
    assert_response :success
    assert_select 'a[href=?]', user_path(user),  {text: "Profile"}
    get user_path(user)
    assert_response :success
    assert_select "td", {text: user.name}
    assert_select 'td', {text: user.email}
    assert_select 'a[href=?]', root_path
  end

  test "profile page should not be accessible if not logged in" do
    get user_path(2)
    assert_select 'a', {text: "Home", count: 0}
    assert_redirected_to root_url
  end

  test "logged in user should not be able to access another user's profile" do
    user = users(:marktwain)
    userOther = users(:tomsawyer)
    login_as user
    follow_redirect!
    get user_path(user)
    assert :success
    assert_select "td", user.name
    get user_path(userOther)
    assert_redirected_to root_url
  end

  test "admin should be able to access another user's profile" do
    user = users(:admin)
    userOther = users(:tomsawyer)
    login_as user
    follow_redirect!
    get user_path(user)
    assert :success
    assert_select "td", user.name
    get user_path(userOther)
    assert :success
    assert_select "td", userOther.name
  end

  test "user should not be able to access the register form if he is logged in" do
    user = users(:marktwain)
    login_as(user)
    follow_redirect!
    get new_user_path
    assert_redirected_to root_url
  end

  test "logged in user should not update other user's profile" do
    user1 = users(:marktwain)
    user2 = users(:anonymous)
    login_as(user1)
    follow_redirect!
    get edit_user_path(user2)
    assert_redirected_to root_url
    patch user_path(user2), params: { user: {name: user2.name, password: user1.password, email: user1.email}}
    assert_redirected_to root_url
    userDup = User.find(user2.id)
    assert_equal(user2, userDup)
  end

  # This test produces an error if user2.email is used
  # test "logged in user should not be able to update his profile with other user's email" do
  #   user1 = users(:marktwain)
  #   user2 = users(:anonymous)
  #   login_as(user1)
  #   follow_redirect!
  #   get edit_user_path(user1)
  #   assert_response :success
  #   patch user_path(user1), params: { user: {email: user2.email, name: user2.name, password: user1.password}}
  #   assert_equal("Profile update failed", flash[:danger])
  #   userDup = User.find(user1.id)
  #   assert_equal(user1.email, userDup.email)
  # end

  test "logged in user can update his profile with valid info" do
    user1 = users(:marktwain)
    user2 = users(:anonymous)
    login_as(user1)
    get edit_user_path(user1)
    assert_response :success
    newMailSt = "twainmark@example.com"
    patch user_path(user1), params: { user: {name: user1.name, password: user1.password, email: newMailSt}}
    assert_equal("Profile update successful", flash[:success])
    userDup = User.find(user1.id)
    assert_equal(newMailSt, userDup.email)
    assert_redirected_to root_url
  end

  test "should be able to get edit profile page" do
    user1 = users(:marktwain)
    login_as(user1)
    get edit_user_path(user1)
    assert_response :success
    assert_select("h1", /Edit/)
    assert_select("input[id=?]", "user_name") do
      assert_select("[value=?]", user1.name)
    end
  end

  test "there should be a link to edit profile on the profile page" do
    user = users(:marktwain)
    login_as user
    get user_path(user)
    assert_select "a[href=?]", edit_user_path(user)
  end

  test "profile page should display user role" do
    user = users(:admin)
    login_as user
    follow_redirect!
    get user_path(user)
    assert_response :success
    assert_select "td", /Admin/
  end

  test "edit profile page should have a link to home" do
    user = users(:tomsawyer)
    login_as(user)
    follow_redirect!
    get edit_user_path(user)
    assert_select "a[href=?]", root_url
  end

  test "admin user should have a link to users index page" do
    user = users(:admin)
    login_as(user)
    follow_redirect!
    assert_select "a[href=?]", users_path
  end

  test "non-admin users should not have a link to a users index page" do
    user = users(:marktwain)
    login_as(user)
    follow_redirect!
    assert_select "a[href=?]", users_path, {count: 0}
  end

  test "admin users should be able to see the users listing at index page" do
    user = users(:admin)
    login_as(user)
    follow_redirect!
    get users_path
    assert_response :success
    User.take(5).each do |usr|
      assert_select "*", /#{usr.name}/
    end
  end

  test "non-admin and non-registered users should not be able to see the users listing at index page" do
    get users_path
    assert_redirected_to root_url
  end

  test "there should be a home link at users index" do
    user = users(:admin)
    login_as(user)
    follow_redirect!
    get users_path
    assert_response :success
    assert_select "a[href=?]", root_path
  end

  test "there should be the correct route for make_facilitator action" do
    assert_recognizes({ controller: 'users', action: 'enable_facilitator',
      id: '2' }, { path: '/users/2/enable_facilitator', method: :post})

    assert_recognizes({ controller: 'users', action: 'disable_facilitator',
      id: '2' }, { path: '/users/2/disable_facilitator', method: :post})
  end

  test "admin should be able to grant facilitator privileges to a user" do
    user = users(:admin)
    userOther = users(:marktwain)
    login_as(user)
    follow_redirect!
    assert(userOther.visitor?, "Mark Twain should be a visitor")
    assert_not(userOther.facilitator?, "Mark Twain should not be facilitator")
    post enable_facilitator_path(userOther)
    assert_redirected_to user_path(userOther)
    userOther = User.find(userOther.id)
    assert(userOther.facilitator?, "Mark Twain should now be facilitator")
    post disable_facilitator_path(userOther)
    assert_redirected_to user_path(userOther)
    userOther = User.find(userOther.id)
    assert_not(userOther.facilitator?, "Mark Twain should not be facilitator any more")
    assert(userOther.visitor?, "Mark Twain should again be a mere visitor")
  end

  test "admin user should have a button to enable facilitator on the user profile page" do
    admin = users(:admin)
    mark = users(:marktwain)
    login_as(admin)
    follow_redirect!
    get user_path(mark)
    assert_select("form[action=?]", enable_facilitator_path(mark)) do
      assert_select("input[type=?]", "submit") do
        assert_select("[value=?]", "Enable facilitator")
      end
    end
  end

  test "admin user should have a button to disable facilitator on the user profile page" do
    admin = users(:admin)
    facilitator = users(:tomsawyer)
    login_as(admin)
    follow_redirect!
    get user_path(facilitator)
    assert_response :success
    assert(facilitator.facilitator?)
    assert_select("td", facilitator.name)
    assert_select('form[action=?]', disable_facilitator_path(facilitator)) do
      assert_select('input[type=?]', "submit") do
        assert_select('[value=?]', "Disable facilitator")
      end
    end
  end

  test "enable/disable facilitator button should not be available at admin user" do
    admin = users(:admin)
    login_as(admin)
    follow_redirect!
    get user_path(admin)
    assert_select("form[action=?]", disable_facilitator_path(admin), {count: 0})
    assert_select("form[action=?]", enable_facilitator_path(admin), {count: 0})
  end

  test "there should not be any enable/disable link on a non-visitor user" do
    admin = users(:admin)
    nonvisitor = users(:nonvisitor)
    login_as(admin)
    follow_redirect!
    get user_path(nonvisitor)
    assert_response :success
    assert_select("form[action=?]", enable_facilitator_path(nonvisitor), {count: 0})
    assert_select("form[action=?]", disable_facilitator_path(nonvisitor), {count: 0})
  end

end #class

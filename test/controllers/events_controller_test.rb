require 'test_helper'

class EventsControllerTest < ActionDispatch::IntegrationTest

  ##########################
  # Tests for create event #
  ##########################

  test "route for create action" do
    assert_recognizes({ controller: 'events', action: 'create',
      group_id: '1' },
      { path: 'groups/1/events', method: 'post' })
  end

  test "event create action should be correct" do
    facilitator = users(:tomsawyer)
    mg = groups(:lacotabo)
    tStart = DateTime.tomorrow + 17.hours
    tEnd = tStart + 3.hours
    login_as facilitator
    #assert_difference "mg.events.count", 1 do
    assert_difference "Event.count", 1 do
      post group_events_path(mg), params: { event: { time_start: tStart, time_end: tEnd }}
    end
    assert_not_nil(flash[:success], "There should be a success message")
    assert_redirected_to mg
    follow_redirect!
    assert_select("p[class=?]", 'success')
  end

  test "event should not be creatable by non-admin user" do
    visitor = users(:marktwain)
    mg = groups(:lacotabo)
    tStart = DateTime.tomorrow + 17.hours
    tEnd = tStart + 3.hours
    login_as visitor
    assert_difference "mg.events.count", 0 do
      post group_events_path(mg), params: { event:
        { time_start: tStart, time_end: tEnd }}
    end
    assert_redirected_to root_url
  end#test

  test "event should not be creatable by non-owning facilitator" do
    facilitator = users(:facilitator2)
    mg = groups(:lacotabo)
    tStart = DateTime.tomorrow + 17.hours
    tEnd = tStart + 3.hours
    login_as facilitator
    assert_difference "mg.events.count", 0 do
      post group_events_path(mg), params: { event:
        { time_start: tStart, time_end: tEnd }}
    end
    assert_redirected_to root_url
  end#test

  test "with overlapping time should not be creatable" do
    facilitator = users(:tomsawyer)
    mg = groups(:lacotabo)
    tStart = DateTime.tomorrow + 17.hours
    tEnd = tStart + 3.hours
    tStartOtr = tStart + 1.hour
    tEndOtr = tEnd - 1.hour
    login_as facilitator
    assert_difference "mg.events.count", 1 do
      post group_events_path(mg), params: { event: { time_start: tStart, time_end: tEnd }}
    end
    assert_no_difference "mg.events.count" do
      post group_events_path(mg), params: { event: { time_start: tStartOtr, time_end: tEndOtr }}
    end
    assert(flash[:danger], "There should be a warning message concerning event time")
  end

  test "events with overlapping time belonging to different groups should be creatable" do
    facilitator = users(:tomsawyer)
    mg = groups(:lacotabo)
    tStart = DateTime.tomorrow + 17.hours
    tEnd = tStart + 3.hours
    tStartOtr = tStart + 1.hour
    tEndOtr = tEnd - 1.hour

    login_as facilitator
    assert_difference "mg.events.count", 1 do
      post group_events_path(mg), params: { event: { time_start: tStart, time_end: tEnd }}
    end
    assert(flash[:success])
    logout

    facilitator = users(:facilitator2)
    mg = groups(:writersgroup)
    login_as facilitator
    assert_difference "mg.events.count", 1 do
      post group_events_path(mg), params: { event: { time_start: tStart, time_end: tEnd }}
      assert(flash[:success])
    end
  end#test


  ######################
  # Tests for new form #
  ######################

  test "there is a route for new" do
    assert_recognizes({ controller: 'events', action: 'new',
      group_id: '1'},
      { path: "groups/1/events/new" , method: 'get'})
  end

  test "new form page - check title" do
    mg = groups(:lacotabo)
    get group_path(mg)
    get new_group_event_url(mg)
    assert_response :success
    assert_select('h1', "New Event")
  end

  test "new form page leads to create action" do
     mg = groups(:lacotabo)
     get new_group_event_path(mg)
     assert_response :success
     assert_select "form[action=?]", group_events_path(mg) do
       assert_select "[method=?]", 'post'
     end
   end

   test "giving wrong time displays error messages" do
      facilitator = users(:tomsawyer)
      mg = groups(:lacotabo)
      login_as facilitator
      get new_group_event_path(mg)
      assert_response :success
      tStart = DateTime.tomorrow + 21.hours
      tEnd = DateTime.tomorrow + 20.hours
      post group_events_path(mg), params: { event: {
        time_start: "#{tStart}", time_end: "#{tEnd}" }}
      assert_select "p[class=?]", "danger"
      assert_select "li[class=?]", "error_item"
    end#test

    test "link leading to new event page is shown to facilitator" do
      facilitator = users(:tomsawyer)
      mg = groups(:lacotabo)
      login_as facilitator
      get group_path(mg)
      assert_select("a[href=?]", new_group_event_path(mg))
    end

end#class

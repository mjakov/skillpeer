require 'test_helper'

class LoginsControllerTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:marktwain)
  end

  test "should get new" do
    get new_login_path
    assert_response :success
    assert_select 'body', /Login/
    assert_select 'h2', /Log in/
  end

  test "successful login" do
    post login_path, params: {email: @user.email, password: @user.password}
    assert_equal(@user.id, session[:current_user_id])
    assert_redirected_to root_url
    follow_redirect!
    assert_match(/Welcome/, flash[:success])
    assert_select 'li#logout_button', {count: 1}
    assert_select 'a', {text: "Login", count: 0}
    # assert that the root url does not display register link
    assert_select 'a', {text: "Register", count: 0}
  end

  test "login should fail because of wrong password" do
    post login_path, params: {email: @user.email, password: ""}
    assert_equal(nil, session[:current_user_id])
    assert_equal("Login failed", flash[:danger])
  end

  test "user should be able to logout" do
    user = users(:marktwain)
    login_as(user)
    delete login_path
    assert_redirected_to root_url
    assert_match(/See you/, flash[:info])
  end

  test "user should not be able to access the login form anew if he is already logged in" do
    user1 = users(:marktwain)
    user2 = users(:anonymous)
    login_as(user1)
    get new_login_path
    assert_redirected_to root_url
  end

  test "user should be able to login once he has logged out" do
    user1 = users(:marktwain)
    user2 = users(:anonymous)
    login_as(user1)
    logout
    follow_redirect!
    assert_match(/See you/, flash[:info])
    get new_login_path
    assert_response :success
  end

  test "if a user is not logged in then logout should display no message" do
    logout
    assert_redirected_to root_url
    follow_redirect!
    assert_nil(flash[:info])
  end

  test "each user should be greeted based on his role" do
    user_visitor = users(:marktwain)
    user_facilitator = users(:tomsawyer)
    user_admin = users(:admin)
    login_as user_visitor
    follow_redirect!
    assert_select "p", /Welcome/
    logout
    follow_redirect!

    login_as user_facilitator
    follow_redirect!
    assert_select "p", /Welcome/
    logout
    follow_redirect!

    login_as user_admin
    follow_redirect!
    assert_select "p", /Welcome/
    logout
    follow_redirect!
  end

end
